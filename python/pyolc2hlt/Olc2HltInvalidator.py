"""
Module containing class Olc2HltInvalidator and related methods.
"""

from __future__ import annotations

import logging
import subprocess

_LOG = logging.getLogger("Olc2HltInvalidator")


class Olc2HltInvalidator:
    """Class implementing invalidation of OLC2HLT conditions.

    Parameters
    ----------
    config_id : str
        ID of the configuration object.
    """

    def __init__(self, config_id: str, testing: bool = False):
        self._config_id = config_id
        self._testing = testing
        self._result: subprocess.CompletedProcess | None = None

    def run(self) -> None:
        """Do invalidation, throws in case of errors."""
        args = ["olc2hlt-invalidate", "-n", self._config_id]
        if self._testing:
            args.insert(0, "/usr/bin/echo")
        _LOG.info("executing: %s", args)
        self._result = subprocess.run(args, check=True)
        _LOG.info("invalidation finished successfully")

    @property
    def exec_result(self) -> subprocess.CompletedProcess | None:
        """
        Returns
        -------
        result : `subprocess.CompletedProcess`
            The result of the process execution, only useful for testing.
        """
        return self._result
