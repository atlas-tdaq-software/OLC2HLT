"""Module which defines Smoother class and related methods.
"""

from __future__ import annotations

import collections
import logging

_LOG = logging.getLogger("olc2hlt")

_LBData = collections.namedtuple("_LBData", "startTime, endTime, value")


class Smoother:
    """Implementation of operations for smoothing fluctuations in the incoming
    lumi values.

    It should normally be used only outside stable beams periods, with stable
    beams input data should be already regular enough.

    Current implementation does smoothing by averaging luminosity numbers from
    last few lumi blocks. Additionally it tries to suppress single-LB spikes
    w.r.t. zero level.

    Parameters
    ----------
    numLB : `int`
        Max. number of the LBs to average.
    maxTime : `float`
        Max. time period to average, seconds.
    doSpikes : `bool`
        If True then try to suppress single-LB spikes.
    """

    def __init__(self, numLB: int, maxTime: float, doSpikes: bool):
        self.numLB = numLB
        self.maxTime = maxTime * 1e9
        self.doSpikes = doSpikes

        self.lbs: list[_LBData] = []

    def addLB(self, startTime: int, endTime: int, value: float) -> None:
        """Add one more LB.

        Parameters
        ----------
        startTime : `int`
            Time when LB started, in ns since epoch.
        endTime : `int`
            Time when LB finished, in ns since epoch.
        value : `float`
            Luminosity value for this LB.
        """
        # append
        self.lbs.append(_LBData(startTime, endTime, value))

        # remove old data, but keep at least one LB
        while len(self.lbs) > self.numLB:
            del self.lbs[0]
        while len(self.lbs) > 1 and (self.lbs[-1].endTime - self.lbs[0].startTime) > self.maxTime:
            del self.lbs[0]

    def reset(self) -> None:
        """Remove all collected values."""
        self.lbs = []

    def value(self) -> float:
        """Return averaged value."""
        if not self.lbs:
            _LOG.warning("Smoother: value() called with empty data, will return 0")
            return 0.0
        if self.doSpikes and len(self.lbs) > 1:
            # count LBs with non-zero data, if there is only one then return 0
            nonEmptyCount = sum(1 for lb in self.lbs if lb.value > 0)
            if nonEmptyCount == 1:
                return 0.0

        # average, weighted by LB length
        lsum = 0.0
        wsum = 0.0
        for lb in self.lbs:
            weight = lb.endTime - lb.startTime
            lsum += lb.value * weight
            wsum += weight

        return lsum / wsum
