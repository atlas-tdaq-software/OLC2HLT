"""Module which defines StableBeamsFlag class and related methods.
"""

from __future__ import annotations

import logging

import ers
import ispy

_LOG = logging.getLogger("olc2hlt")


class StableBeamsFlag:
    """
    Class which defines callable which can be used to check the instantaneous
    value of LHC-declared stable beams flag.

    Parameters
    ----------
    sbeamPart : str
        Name of the partition containg IS server for stable beams flag.
    sbeamInfo : str
        Name of the stable beans IS information object (including server name).
    """

    ISObjectClass = ispy.ISObject  # can be changed for testing

    def __init__(self, sbeamPart: str, sbeamInfo: str):
        self.sbeamPart = sbeamPart
        self.sbeamInfo = sbeamInfo

        _LOG.info("Configured StableBeamsFlag: partition=%s info=%s", self.sbeamPart, self.sbeamInfo)

    def __call__(self) -> bool:
        """Return True if stable beams flag is set.

        Current implementation is based on polling, not the most efficient
        but probably most reliable. Polling happens once per LB, so it should
        not be too bad.

        Returns
        -------
        flag : bool
            True for stable beams, False if no stable beams or if state
            cannot be determined.
        """
        try:
            isobj = self.ISObjectClass(self.sbeamPart, self.sbeamInfo, "DdcIntInfo")
            isobj.checkout()
        except ers.Issue as exc:
            _LOG.warning("Problem with IS server for stable beam flag: %s", exc)
            # cannot tell if it's stable or not, assume non-stable
            return False

        value = bool(isobj.value)
        _LOG.debug("StableBeamsFlag: value=%s (%s - %s)", value, self.sbeamPart, self.sbeamInfo)

        return value
