"""Module which defines Decider class and related methods
"""

from __future__ import annotations

import logging
from collections.abc import Callable
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from ._types import OnlPrefLumi, _Decider

_LOG = logging.getLogger("olc2hlt")


class Decider:
    """Decider class is responsible for making decision about the need
    to update luminosity value in HLT (via CTP).

    This class provides implementation that delegates decision to one
    of the two other dicides, one decider is used for stable beams
    situations and another for non-stable beams.
    """

    def __init__(
        self,
        stableDecider: _Decider,
        unstableDecider: _Decider,
        minLBLength: float,
        sbeamFlag: Callable[[], bool],
    ):
        """Make new instance.

        @param stableDecider:   Instance of decider used for stable beams
        @param unstableDecider:   Instance of decider used outside stable beams
        @param minLBLength: Minimal LB length, shorther LBs will be ignored
        @param sbeamFlag: Callable instance used to check stable beams flag
                        value.
        """
        self.stableDecider = stableDecider
        self.unstableDecider = unstableDecider
        self.minLBLength = minLBLength
        self.sbeamFlag = sbeamFlag

        self.lastStable: bool | None = None

    def decide(self, oldData: OnlPrefLumi | None, newData: OnlPrefLumi) -> bool:
        """Decide if update is needed.

        @param oldData:   Old data that was published
        @param newData:   Data for current LB
        """
        stableFlag = self.sbeamFlag()
        lastStable = self.lastStable
        if lastStable is None:
            _LOG.info("decider: stable flag: %s", stableFlag)
        self.lastStable = stableFlag

        # if there is nothing to compare it to then just store
        if oldData is None:
            _LOG.info("decider: no old data, forcing update")
            return True

        # ignore very short LBs
        lbLenSec = (newData.EndTime - newData.StartTime) / 1e9
        if lbLenSec < self.minLBLength:
            _LOG.info("decider: ignore short LB: %s sec (min length: %s sec)", lbLenSec, self.minLBLength)
            return False

        result = False
        if stableFlag:
            if not lastStable:
                # changed from unstable to stable
                _LOG.info("decider: stable beams declared, forcing update")
                result = True
                # also reset unstable decider
                self.unstableDecider.reset()
            else:
                # call stable decider
                result = self.stableDecider.decide(oldData, newData)
        else:
            if lastStable:
                # changed from unstable to stable
                _LOG.info("decider: stable beams flag reset")
                # reset stable decider
                self.stableDecider.reset()
            # call unstable decider
            result = self.unstableDecider.decide(oldData, newData)

        return result
