"""
Implementation of the RunControl interface for OLC2HLT.
"""

from __future__ import annotations

import logging
from typing import TYPE_CHECKING

from rcpy import Controllable

from .Config import Config
from .LogHandler import initLogging

if TYPE_CHECKING:
    from .Olc2Hlt import Olc2Hlt
    from .Olc2HltInvalidator import Olc2HltInvalidator

initLogging()

_LOG = logging.getLogger("Olc2HltControllable")


class Olc2HltControllable(Controllable):
    """
    Implementation of controlable interface from RunControl.
    """

    def __init__(self) -> None:
        Controllable.__init__(self)

        self._olc2hlt: Olc2Hlt | None = None
        self._sor_invalidator: Olc2HltInvalidator | None = None
        self._eor_invalidator: Olc2HltInvalidator | None = None
        self._running: bool | None = None

    def configure(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in configure: %s", cmd)

        # read configuration, this may throw to fail transition
        config = Config()
        self._olc2hlt = config.makeApp()
        self._sor_invalidator = config.makeSoRInvalidator()
        self._eor_invalidator = config.makeEoRInvalidator()

    def connect(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def prepareForRun(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in prepareForRun: %s", cmd)

        # do SoR invalidation, it can throw an exception
        if self._sor_invalidator:
            # if exception happens here it means that invalidation has failed,
            # if we allow exception to leak then most likely this application
            # will be taken out and we don't want that. We still want to alert
            # others about failure but using ERS from Python is problematic so
            # the hope here is that error is already sent to ERS and we just
            # print a message to a log here.
            try:
                self._sor_invalidator.run()
            except Exception:
                _LOG.error("OLC2HLT invalidation failed", exc_info=True)

        # subscribe to IS and start asynchronous loop,
        # this can throw to signal transition error
        if self._olc2hlt:
            self._olc2hlt.start()

        self._running = True

    def stopROIB(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in stopROIB: %s", cmd)

        self._running = False

        # stop subscriber, this should wait until all publishing is done
        if self._olc2hlt:
            self._olc2hlt.stop()

        # do EoR invalidation, this can throw and fail transition
        if self._eor_invalidator:
            self._eor_invalidator.run()

    def stopDC(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopHLT(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopRecording(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopGathering(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def stopArchiving(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def disconnect(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """

    def unconfigure(self, cmd: object) -> None:
        """
        @param cmd: instance of TransitionCmd type
        """
        _LOG.info("in unconfigure: %s", cmd)
        self._olc2hlt = None
        self._sor_invalidator = None
        self._eor_invalidator = None

    def user(self, cmd: object) -> None:
        """
        @param cmd: instance of UserCmd type
        """

    def onExit(self, state: object) -> None:
        """
        @param state: instance of FSM_STATE type
        """
        _LOG.info("in onExit: %s", state)

    def publish(self) -> None:
        """
        Publish (incremental) statistics, may be called periodically
        """
