"""Module which defines SmootherDecider class and related methods.
"""

from __future__ import annotations

import logging
from typing import TYPE_CHECKING

from .Smoother import Smoother

if TYPE_CHECKING:
    from ._types import OnlPrefLumi

_LOG = logging.getLogger("olc2hlt")


class SmootherDecider:
    """Decider class is responsible for making decision about the need to
    update luminosity value in HLT (via CTP).

    This class is supposed to be used with non-stable beams. It does smoothing
    of the lumi data from several consequitive LBs and triggers update when
    averaged from that smoothing differs from previous lumi value by
    significant amount.

    Parameters
    ----------
    numLB : `int`
        Max. number of the LBs to average.
    maxTime : `float`
        Max. time period to average, seconds.
    doSpikes : `bool`
        If True then try to suppress single-LB spikes.
    lumiChangePercent : `float`
        Relative change _in percent_ which triggers update.
    """

    def __init__(self, numLB: int, maxTime: float, doSpikes: bool, lumiChangePercent: float):
        self.lumiChangePercent = lumiChangePercent
        self.smoother = Smoother(numLB, maxTime, doSpikes)
        self.value: float | None = None  # current average, only for testing

    def decide(self, oldData: OnlPrefLumi, newData: OnlPrefLumi) -> bool:
        """Return True if update is needed.

        This method is called repeatedly for each new LB.

        Parameters
        ----------
        oldData : `ISObject`
            Previous luminosity information, `LBAvInstLumi` attribute used to
            access luminosity value.
        newData : `ISObject`
            Updated luminosity information, `LBAvInstLumi` attribute used to
            access luminosity value.

        Returns
        -------
        update : `bool`
            True if update is needed.
        """
        oldLumi = oldData.LBAvInstLumi
        newLumi = newData.LBAvInstLumi

        self.smoother.addLB(newData.StartTime, newData.EndTime, newData.LBAvInstLumi)
        self.value = self.smoother.value()

        if oldLumi == newLumi:
            # If the numbers are the same then there is nothing to do
            result = False
            _LOG.info("SmootherDecider: oldLumi=%s newLumi=%s result=%s", oldLumi, self.value, result)
            return result
        elif oldLumi == 0:
            # old lumi was 0 then update when new lumi is not 0
            result = self.value != 0
            _LOG.info("SmootherDecider: oldLumi=%s newLumi=%s result=%s", oldLumi, self.value, result)
            return result
        else:
            # update if relative change is larger than threshold
            relDelta = (self.value - oldLumi) / oldLumi * 100
            result = abs(relDelta) >= self.lumiChangePercent
            _LOG.info(
                "SmootherDecider: oldLumi=%s newLumi=%s relDelta=%.2f%% result=%s",
                oldLumi,
                self.value,
                relDelta,
                result,
            )
            return result

    def reset(self) -> None:
        """Reset state, called when switching to stable beams."""
        self.smoother.reset()
