"""Module which defines Olc2Hlt class and related methods."""

from __future__ import annotations

import logging
import os
import queue
import threading
import time
from typing import TYPE_CHECKING, Any

import ers
import ispy
from cool_utils import CTPUpdateConditions, FolderIndex

if TYPE_CHECKING:
    from ._types import OnlPrefLumi
    from .Decider import Decider
    from .ISReceiver import ISReceiver

_LOG = logging.getLogger("olc2hlt")


class CTPError(ers.Issue):
    """ERS Issue class for CTP-related errors"""

    def __init__(self, message: str, cause: Any = None, **kw: Any):
        ers.Issue.__init__(
            self,
            'Failure executing command: ' + message,
            kw,
            cause if isinstance(cause, ers.Issue) else None
        )


class Olc2Hlt:
    """Main application class of olc2hlt application.

    Parameters
    ----------
    inputQueue : `Queue.Queue`
        Queue for input data (source IS objects).
    dstPart : str
        Name of the partition containg IS server for output data.
    dstInfo : str
        Name of the output IS information object.
    decider :
        Object responsible for update decisions
    noCTPUpdate : bool
        If true then pass -N option to ctpUpdateCommand
    receiver : ISReceiver
        Instance of ISReceiver class
    ctp_controller : str
        Name of the CTP controller application.
    """

    ISObjectClass = ispy.ISObject  # can be changed for testing

    def __init__(
        self,
        inputQueue: queue.Queue,
        dstPart: str,
        dstInfo: str,
        decider: Decider,
        noCTPUpdate: bool,
        receiver: ISReceiver,
        ctp_partition: str | None = None,
        ctp_controller: str = "CtpController",
    ):
        self.inputQueue = inputQueue
        self.dstPart = dstPart
        self.dstInfo = dstInfo
        self.decider = decider
        self.noCTPUpdate = noCTPUpdate
        self.receiver = receiver

        self.wakeupInterval = 10  # in seconds
        self.lastWakeup: float = 0.0

        self.thread: threading.Thread | None = None

        self.ctp_partition = ctp_partition or os.environ["TDAQ_PARTITION"]
        self.ctp_controller = ctp_controller
        self._ctp_commander: CTPUpdateConditions | None = None

    def run(self) -> None:
        """Run the whole shebang and never return, this is main method of the
        whole application.
        """
        # tell receiver to start getting data from IS
        self.receiver.subscribe()

        # run (forever)
        self._loop()

        # tell receiver to stop receiving data
        self.receiver.unsubscribe()

    def start(self) -> None:
        """Start event loop in a separate thread and return."""
        # tell receiver to start getting data from IS
        self.receiver.subscribe()

        # start a thread
        self.thread = threading.Thread(target=self._loop)
        self.thread.start()
        _LOG.info("Started background thread")

    def stop(self) -> None:
        """Stop a thread with event loop."""
        if not self.thread:
            _LOG.warning("Background thread is not running")
            return

        # tell receiver to stop receiving data
        self.receiver.unsubscribe()

        # put special token into queue to signal that no more items is expected
        item = None
        _LOG.info("Adding stop flag to input queue")
        self.inputQueue.put(item, True)

        # wait for the thread to finish
        _LOG.info("Waiting the background thread to stop")
        self.thread.join()
        self.thread = None
        _LOG.info("Background thread finished")

    def _loop(self) -> None:
        # find out what was the last data that we sent to HLT
        oldData = self._restoreData()

        # loop and wait for data
        while True:
            try:
                item = self.inputQueue.get(block=True, timeout=self.wakeupInterval)
            except queue.Empty:
                # on timeout just call wakeup
                self._wakeups()
                continue

            # someone could push None into queue to force clean stop
            if item is None:
                self.inputQueue.task_done()
                break

            # item is a tuple of IS reason and IS data
            reason, data = item

            # if IS info or server is gone do not do anyhting
            if reason == ispy.Reason.Deleted:
                _LOG.warning("Source IS info object has disappeared, will wait until it returns")
                self.inputQueue.task_done()
                continue

            _LOG.info(
                "Received new LB: run=%s LBN=%s lumi=%s",
                data.RunNumber,
                data.LBN,
                data.LBAvInstLumi,
            )

            # see if data has changed
            if self.decider.decide(oldData, data):
                # update HLT and remember it
                if self._sendToHLT(data):
                    oldData = data

            self.inputQueue.task_done()

            # do wakeups if necessary
            self._wakeups()

    def _restoreData(self) -> OnlPrefLumi | None:
        """
        Try to restore data most recently sent to HLT. There could be several
        options for that:
        1. look at the conditions data that CTP handler stored most recently,
           for that we need to know what kind of intervals CTP handler creates
        2. look at IS data in the destination IS info object (if it exists),
           not 100% reliable because CTP can refuse to use that data
        3. store most recent data in some external source (file) and recover
           from there

        It's actually not clear yet if this needs to be done at all, so for now
        we do not restore anything which means that we are forcing update at
        the start.
        """
        return None

    def _wakeups(self) -> None:
        """Do some periodic work."""
        now = time.time()
        if now - self.lastWakeup > self.wakeupInterval - 0.01:
            # currently there is nothing to do

            # wakeups can take some time, do not count that
            self.lastWakeup = time.time()

    def _sendToHLT(self, data: OnlPrefLumi) -> bool:
        """Store the data in destination IS server and notify CTP."""
        try:
            isobj = self.ISObjectClass(self.dstPart, self.dstInfo, "OnlPrefLumi")
        except UserWarning as exc:
            _LOG.warning("Problem with server for publishing to IS: %s", exc)
            return False

        try:
            # fill all data members
            isobj.RunNumber = data.RunNumber
            isobj.LBN = data.LBN
            isobj.StartTime = data.StartTime
            isobj.EndTime = data.EndTime
            isobj.Channel = data.Channel
            isobj.AlgorithmID = data.AlgorithmID
            isobj.LBAvInstLumi = data.LBAvInstLumi
            isobj.LBAvEvtsPerBX = data.LBAvEvtsPerBX
            isobj.LumiType = data.LumiType
            isobj.Valid = data.Valid
            isobj.BunchInstLumi = data.BunchInstLumi

            # publish
            isobj.checkin()

            _LOG.info(
                "Publishing OnlPrefLumi to IS: RunNumber=%s LBN=%s Channel=%s "
                "LBAvInstLumi=%s LBAvEvtsPerBX=%s Valid=%s",
                isobj.RunNumber,
                isobj.LBN,
                isobj.Channel,
                isobj.LBAvInstLumi,
                isobj.LBAvEvtsPerBX,
                isobj.Valid,
            )

        except Exception as exc:
            _LOG.warning("Problem publishing status info to IS: %s", exc)
            return False

        if self.noCTPUpdate:
            print("Skipping actual CTP update due to no-update option")
            return True

        # Notify CTP that new conditions are ready.
        try:
            self.ctp_commander.update(FolderIndex.LUMINOSITY)
        except ers.Issue as exc:
            # Do not report some errors from CTP, we want better more specific error
            # messages, but for now just restore the situation when CTP error
            # were completely ignored.
            err = str(exc)
            ignore_msg = (
                "An error occurred during command: set conditions update. Reason: CommandExecutionFailed"
            )
            if ignore_msg in err:
                _LOG.info("CTP command failed due to possible duplicate request %s", err)
                return False
            else:
                issue = CTPError(err.strip(), exc)
                ers.error(issue)
                raise issue

        return True

    @property
    def ctp_commander(self) -> CTPUpdateConditions:
        if self._ctp_commander is None:
            self._ctp_commander = CTPUpdateConditions(self.ctp_partition, self.ctp_controller)
        return self._ctp_commander
