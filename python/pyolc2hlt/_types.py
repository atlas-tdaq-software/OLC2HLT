from __future__ import annotations

from collections.abc import Sequence
from typing import Protocol


class OnlPrefLumi(Protocol):
    """Python match for IS OnlPrefLumi structure."""

    RunNumber: int
    LBN: int
    StartTime: int
    EndTime: int
    Channel: int
    AlgorithmID: int
    LBAvInstLumi: float
    LBAvEvtsPerBX: float
    LumiType: int
    Valid: int
    BunchInstLumi: Sequence[float]


class _Decider(Protocol):
    """Interface for various decider classes."""

    def decide(self, oldData: OnlPrefLumi, newData: OnlPrefLumi) -> bool:
        ...

    def reset(self) -> None:
        ...
