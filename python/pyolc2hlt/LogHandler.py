"""
Module containing LogHandler class and related methods
"""

from __future__ import annotations

import logging.handlers
import sys
from typing import TextIO

_oneTime = False


def initLogging() -> None:
    """
    Initialize logging system according to ATLAS "standards"
    """
    global _oneTime
    if not _oneTime:
        root = logging.getLogger()
        root.setLevel(logging.INFO)
        formatter = logging.Formatter("%(asctime)s [%(levelname)s] %(name)s: %(message)s")
        handler = LogHandler()
        handler.setFormatter(formatter)
        root.addHandler(handler)
        _oneTime = True


class LogHandler(logging.StreamHandler):
    """
    Special handler class which sends INFO and DEBUG messages to stdout and
    everything else to stderr.
    """

    def __init__(self) -> None:
        """Make new instance of LogHandler."""
        logging.StreamHandler.__init__(self)
        self.streams: set[TextIO] = set()

    def flush(self) -> None:
        """
        Flushes the streams (all that we actually used).
        """
        self.acquire()
        try:
            for stream in self.streams:
                stream.flush()
            self.streams.clear()
        finally:
            self.release()

    def emit(self, record: logging.LogRecord) -> None:
        """
        Emit a record, set stream based on severity and call base class.
        """
        if record.levelno >= logging.WARN:
            # warning and above
            self.stream = sys.stderr
        else:
            self.stream = sys.stdout
        self.streams.add(self.stream)
        logging.StreamHandler.emit(self, record)
