"""Module containing ISReceiver class and related methods."""

from __future__ import annotations

import logging
import queue

import ispy

_LOG = logging.getLogger("is_receive")


class ISReceiver:
    """Instances of this class handle IS subscriptions for single IS object.

    Parameters
    ----------
    partition : str
        Name of the partition for IS server.
    isinfo : str
        IS info object name, including server name.
    queue : Queue.Queue
        Queue where the IS data will be pushed.
    """

    ISInfoReceiverClass = ispy.ISInfoReceiver  # can be changed for testing
    ISInfoDynAnyClass = ispy.ISInfoDynAny  # can be changed for testing

    def __init__(self, partition: str, isinfo: str, queue: queue.Queue):
        self.partition = partition
        self.isinfo = isinfo
        self.queue = queue
        self.receiver: ispy.ISInfoReceiver | None = None
        # seconds to wait for queue, should not be too long, otherwise IS will
        # start complaining
        self.timeout = 3
        self.serialize_callbacks = True

    def subscribe(self) -> None:
        """Subscribe to IS data."""
        part = ispy.IPCPartition(self.partition)
        self.receiver = self.ISInfoReceiverClass(part, self.serialize_callbacks)

        # subscribe to each name individually
        _LOG.info("Subscribing to %s in partition %s", self.isinfo, self.partition)
        try:
            event_mask = [
                ispy.Reason.Created,
                ispy.Reason.Updated,
                ispy.Reason.Subscribed,
                ispy.Reason.Deleted,
            ]
            self.receiver.scheduleSubscription_info(self.isinfo, self._callback, event_mask)
        except UserWarning as exc:
            _LOG.error(
                "Subscription to %s in partition %s failed: %s",
                self.isinfo,
                self.partition,
                exc,
            )
            raise

    def unsubscribe(self) -> None:
        """Unsubscribe from everything."""
        try:
            if self.receiver is not None:
                self.receiver.unsubscribe(self.isinfo, True)
        except Exception as exc:
            # unsubscribe can fail sometimes, don't let exception destroy the
            # rest of it
            _LOG.warning("Unsubscribe (%s in %s) failed: %s", self.isinfo, self.partition, exc)

    def _callback(self, cinfo: ispy.ISCallbackInfo) -> None:
        """Handle subscription to single info.

        Parameters
        ----------
        cinfo : `ispy.ISCallbackInfo`
            Callback info object.
        """
        _LOG.debug(
            "ISInfo: %s  reason: %s  type: %s  time: %s",
            cinfo.name(),
            cinfo.reason(),
            cinfo.type(),
            cinfo.time(),
        )

        val = ispy.ISInfoDynAny()
        cinfo.value(val)

        # item to push into a queue is the tuple (reason, value)
        item = cinfo.reason(), val

        # try to put it into a queue but do not get stuck forever
        try:
            self.queue.put(item, True, self.timeout)
            _LOG.debug("Queue size: %s", self.queue.qsize())
        except queue.Full:
            _LOG.warning("Queue is full, ignoring data item %s", item[1])
