"""Module which defines ThresholdDecider class and related methods.
"""

from __future__ import annotations

import logging
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from ._types import OnlPrefLumi

_LOG = logging.getLogger("olc2hlt")


class ThresholdDecider:
    """Decider class is responsible for making decision about the need to
    update luminosity value in HLT (via CTP).

    This class is supposed to be used with non-stable beams. It considers all
    lumi values below certain threshold as zero, for values above threshold
    it makes decisions based on relative change.

    Parameters
    ----------
    threshold : `float`
        Luminosity values below this are considered zeros.
    lumiChangePercent : `float`
        Relative change _in percent_ which triggers update.
    """

    def __init__(self, threshold: float, lumiChangePercent: float):
        self.threshold = threshold
        self.lumiChangePercent = lumiChangePercent

        _LOG.info(
            "Configured ThresholdDecider: threshold=%s lumiChangePercent=%s",
            self.threshold,
            self.lumiChangePercent,
        )

    def decide(self, oldData: OnlPrefLumi, newData: OnlPrefLumi) -> bool:
        """Return True if update is needed.

        This method is called repeatedly for each new LB.

        Parameters
        ----------
        oldData : `ISObject`
            Previous luminosity information, `LBAvInstLumi` attribute used to
            access luminosity value.
        newData : `ISObject`
            Updated luminosity information, `LBAvInstLumi` attribute used to
            access luminosity value.

        Returns
        -------
        update : `bool`
            True if update is needed.
        """
        oldLumi = oldData.LBAvInstLumi
        if oldLumi <= self.threshold:
            oldLumi = 0.0

        newLumi = newData.LBAvInstLumi
        if newLumi <= self.threshold:
            newLumi = 0.0

        if newLumi == oldLumi:
            result = False
            _LOG.info("ThresholdDecider: oldLumi=%s newLumi=%s result=%s", oldLumi, newLumi, result)
            return result
        elif oldLumi == 0:
            # old lumi was 0 then update when new lumi is not 0
            result = True
            _LOG.info("ThresholdDecider: oldLumi=%s newLumi=%s result=%s", oldLumi, newLumi, result)
            return result
        else:
            # update if relative change is larger than threshold
            relDelta = (newLumi - oldLumi) / oldLumi * 100
            result = abs(relDelta) >= self.lumiChangePercent
            _LOG.info(
                "ThresholdDecider: oldLumi=%s newLumi=%s relDelta=%.2f%% result=%s",
                oldLumi,
                newLumi,
                relDelta,
                result,
            )
            return result

    def reset(self) -> None:
        """Reset state, called when switching from stable beams."""
        pass
