"""Module which controls OLC2HLT configuration.
"""

from __future__ import annotations

import logging
import os
import queue

import config

from .Decider import Decider
from .ISReceiver import ISReceiver
from .Olc2Hlt import Olc2Hlt
from .Olc2HltInvalidator import Olc2HltInvalidator
from .StableBeamsFlag import StableBeamsFlag
from .StableDecider import StableDecider
from .ThresholdDecider import ThresholdDecider

_LOG = logging.getLogger("config")

# map of logging level names to level nmbers
_logLvlNames = {logging.getLevelName(lvl): lvl for lvl in (logging.DEBUG, logging.INFO, logging.WARN)}


class Config:
    """Class which reads OLC2HLT configuration from OKS and initializes
    OLC2HLT application from that configuration.
    """

    def __init__(
        self, oks_db: str | None = None, config_id: str | None = None, oks_config_class: str = "OLC2HLTConfig"
    ):
        """Make an instance of Config class.

        @param oks_db:  OKS configuration, e.g. oksconfig:/path/file.data.xml
        @param config_id:  ID of the OLC2HLT configuration in OKS
        """
        oks_db = oks_db or os.environ.get("TDAQ_DB")
        if not oks_db:
            raise LookupError("TDAQ_DB env.var. is missing.")

        self.config_id = config_id or os.environ.get("TDAQ_APPLICATION_NAME")
        if not self.config_id:
            raise LookupError("TDAQ_APPLICATION_NAME env.var. is missing.")

        self.oks_config_class = oks_config_class

        # find app instance in a database
        try:
            db = config.Configuration(oks_db)
            self._cfg = db.get_obj(self.oks_config_class, self.config_id)
        except RuntimeError:
            _LOG.critical(
                "Error reading OKS configuration object %s@%s",
                self.config_id,
                self.oks_config_class,
            )
            raise

    def makeApp(self) -> Olc2Hlt:
        """Make an instance of application from configuration parameters."""
        cfg = self._cfg

        lvl = _logLvlNames.get(cfg["LogLevel"], logging.INFO)
        logging.getLogger().setLevel(lvl)

        # loging to ERS?
        if cfg["Log2ERS"]:
            import ers

            ers.replaceAllLoggingHandlers()

        # start parsing general parameters
        _LOG.info("Reading OKS configuration %s@%s", self.config_id, self.oks_config_class)

        currentPart = os.environ.get("TDAQ_PARTITION") or ""

        # IS source
        srcPart = cfg["SrcPartition"] or currentPart
        srcInfo = cfg["SrcISInfo"]

        # IS destination
        dstPart = cfg["DstPartition"] or currentPart
        dstInfo = cfg["DstISInfo"]

        # revative change in lumi
        lumiChangePercent = cfg["LumiChangePercent"]

        # minimum LS length
        minLBLength = cfg["MinLBLength"]

        noCTPUpdate = cfg["NoCTPUpdate"]

        # Queue for ISinfo data read from IS.
        queueSize = 0
        isQueue: queue.Queue = queue.Queue(queueSize)

        # subscribe to IS source info
        receiver = ISReceiver(srcPart, srcInfo, isQueue)

        # stable beams IS info object
        try:
            sbeamPart = cfg["StableBeamsPartition"]
        except KeyError:
            sbeamPart = "initial"
        try:
            sbeamInfo = cfg["StableBeamsISInfo"]
        except KeyError:
            sbeamInfo = "LHC.StableBeamsFlag"
        sbeamFlag = StableBeamsFlag(sbeamPart, sbeamInfo)

        # stable beams IS info object
        try:
            unstableDeadBand = cfg["UnstableDeadBand"]
        except KeyError:
            unstableDeadBand = 0.01
        try:
            lumiChangePercentUnstable = cfg["LumiChangePercentUnstable"]
        except KeyError:
            lumiChangePercentUnstable = 30

        stableDecider = StableDecider(lumiChangePercent)
        unstableDecider = ThresholdDecider(unstableDeadBand, lumiChangePercentUnstable)
        decider = Decider(stableDecider, unstableDecider, minLBLength, sbeamFlag)

        # make an app instance
        app = Olc2Hlt(isQueue, dstPart, dstInfo, decider, noCTPUpdate, receiver)

        return app

    def makeSoRInvalidator(self) -> Olc2HltInvalidator | None:
        """
        Make instance of invalidator for SoR invalidation
        """
        return self._makeInvalidator("SoRInvalidation")

    def makeEoRInvalidator(self) -> Olc2HltInvalidator | None:
        """Make instance of invalidator for EoR invalidation."""
        return self._makeInvalidator("EoRInvalidation")

    def _makeInvalidator(self, rel_name: str) -> Olc2HltInvalidator | None:
        """
        Make instance of invalidator based on relation name.

        @param rel_name: name of the relation defining invalidator
            configuration
        """
        inv_config = self._cfg[rel_name]
        if not inv_config:
            _LOG.info(f"Invalidator config {rel_name} is not defined")
            return None

        return Olc2HltInvalidator(inv_config.UID())
