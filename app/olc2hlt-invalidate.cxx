//
// $Id: dump_config.cxx 112519 2013-04-18 06:52:40Z salnikov $
//

//-----------------
// C/C++ Headers --
//-----------------
#include <cstdlib>
#include <string>
#include <boost/program_options.hpp>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "config/Configuration.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolUtils/CoolReadable.h"
#include "CoolUtils/CoolReader.h"
#include "CoolUtils/CoolWritable.h"
#include "CoolUtils/CoolWriter.h"
#include "dal/util.h"
#include "ers/ers.h"
#include "ipc/core.h"
#include "is/infodictionary.h"
#include "OLC2HLT/Olc2HltCommand.h"
#include "OLC2HLT/OLC2HLTInvalidateConfig.h"
#include "rc/RunParams.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace po = boost::program_options;
using namespace daq::coolutils;

namespace {

ERS_DECLARE_ISSUE(errors, UncaughtException, "Uncaught exception, terminating.", )

ERS_DECLARE_ISSUE(errors, CommandLineError,
        "command line error: " << error, ((std::string) error))

ERS_DECLARE_ISSUE(warnings, NotWithCmdLine,
        "warning: option --" << name << " is ignored when --cmd-line is specified",
        ((std::string) name))

ERS_DECLARE_ISSUE(warnings, NotWithoutCmdLine,
        "warning: option --" << name << " is ignored without --cmd-line option",
        ((std::string) name))

ERS_DECLARE_ISSUE(errors, PartitionNotFound,
        "partition " << partition << " not found in configuration db",
        ((std::string) partition))

ERS_DECLARE_ISSUE(errors, ConfigNotFound,
        "configuration object " << name << "@" <<
        OLC2HLT::OLC2HLTInvalidateConfig::s_class_name << " not found in configuration db",
        ((std::string) name))

ERS_DECLARE_ISSUE(errors, IntervalNotFound,
        "failed to locate COOL interval for run=" << run << " and LB=" << lbn,
        ((int) run)((unsigned) lbn))

ERS_DECLARE_ISSUE(errors, IntervalNotOpen,
        "attempt to invalidate closed interval (run=" << sinceRun << ", LB="
        << sinceLbn << ") to (run=" << untilRun << ", LB=" << untilLbn << ")",
        ((int) sinceRun)((unsigned) sinceLbn)((int) untilRun)((unsigned) untilLbn))

ERS_DECLARE_ISSUE(errors, RunDiffTooLarge,
        "Run number difference too large, last stored run=" << oldRun << " and new run=" << newRun,
        ((int) oldRun)((int) newRun))

ERS_DECLARE_ISSUE(errors, StoreFailed,
        "Failed to store new COOL record, check log for messages",)

//
//  Implementation of CoolWritable for CoolUtils
//
class Olc2HltWritable : public daq::coolutils::CoolWritable {
public:
    Olc2HltWritable(double mu, unsigned valid) : m_mu(mu), m_valid(valid) {}

    // These methods are implemented for being CoolWritable
    virtual void fillRecord( cool::Record& record ) const;
    virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const;

private:
    double m_mu;
    unsigned m_valid;
};

void Olc2HltWritable::setRecordSpec(cool::RecordSpecification& recordSpec) const
{
    recordSpec.extend("AlgorithmID", cool::StorageType::UInt32);
    recordSpec.extend("LBAvInstLumi", cool::StorageType::Float);
    recordSpec.extend("LBAvEvtsPerBX", cool::StorageType::Float);
    recordSpec.extend("LumiType", cool::StorageType::UInt32);
    recordSpec.extend("Valid", cool::StorageType::UInt32);
    recordSpec.extend("BunchInstLumi", cool::StorageType::Blob64k);
}

void Olc2HltWritable::fillRecord(cool::Record& record) const
{
    record["AlgorithmID"].setValue<cool::UInt32>(0);
    record["LBAvInstLumi"].setValue<cool::Float>(0.);
    record["LBAvEvtsPerBX"].setValue<cool::Float>(m_mu);
    record["LumiType"].setValue<cool::UInt32>(0);
    record["Valid"].setValue<cool::UInt32>(m_valid);
    coral::Blob blob;
    record["BunchInstLumi"].setValue<cool::Blob64k>(blob);
}


class Config {
public:

    Config(int argc, char **argv);

    CoolReader coolReader() const { return CoolReader(m_coolDb, m_coolFolder, m_coolTag); }
    CoolWriter coolWriter() const { return CoolWriter(m_coolDb, m_coolFolder, m_coolTag); }

    // provide read-only access to config options
    std::string const& runMode = m_runMode;
    int const& runNumber = m_runNumber;
    unsigned const& lbNumber = m_lbNumber;
    double const& mu = m_mu;
    unsigned const& valid = m_valid;
    std::string const& isPartition = m_isPartition;
    std::string const& isObject = m_isObject;
    unsigned const& maxRunDiff = m_maxRunDiff;
    std::string const& coolDb = m_coolDb;
    std::string const& coolFolder = m_coolFolder;
    std::string const& coolTag = m_coolTag;
    unsigned const& coolChannel = m_coolChannel;

private:

    // return string from environment or empty string
    static std::string env_string(const char* env);

    po::variables_map parse_cmdline(int argc, char **argv);
    void check_options(po::variables_map& vm);
    void read_oks_config();

    std::string m_partition;   ///< Partition for OKS config
    std::string m_name;   ///< application name for OKS config
    std::string m_database;   ///< configuration database (OKS)

    std::string m_runMode;    ///<  SoR or EoR
    int m_runNumber = -1;
    unsigned m_lbNumber;
    double m_mu;
    unsigned m_valid;
    std::string m_isPartition;
    std::string m_isObject;
    unsigned m_maxRunDiff;

    std::string m_coolDb;
    std::string m_coolFolder;
    std::string m_coolTag;
    unsigned m_coolChannel;
};

Config::Config(int argc, char **argv)
{
    po::variables_map vm = parse_cmdline(argc, argv);

    if (vm.count("cmd-line") == 0) {
        // read configuration from OKS
        read_oks_config();
    }

    ERS_LOG("Configured from " << (vm.count("cmd-line") == 0 ? "OKS:" : "command line:"));
    ERS_LOG("  Run Mode     = " << m_runMode);
    ERS_LOG("  Run Number   = " << m_runNumber);
    ERS_LOG("  LB Number    = " << m_lbNumber);
    ERS_LOG("  Mu           = " << m_mu);
    ERS_LOG("  Valid        = " << m_valid);
    ERS_LOG("  IS Partition = " << m_isPartition);
    ERS_LOG("  IS Object    = " << m_isObject);
    ERS_LOG("  COOL Db      = " << m_coolDb);
    ERS_LOG("  COOL Folder  = " << m_coolFolder);
    ERS_LOG("  COOL Tag     = " << m_coolTag);
    ERS_LOG("  COOL Channel = " << m_coolChannel);
}

// return string from environment or empty string
std::string Config::env_string(const char* env)
{
    const char* value = std::getenv(env);
    if (value == nullptr) value = "";
    return std::string(value);
}


po::variables_map Config::parse_cmdline(int argc, char **argv)
{
    // command line
    po::options_description oks_options("OKS options (not used with --cmd-line)");
    oks_options.add_options()
        ("partition,p",
         po::value(&m_partition)->value_name("STRING")->default_value(env_string("TDAQ_PARTITION")),
         "Name of the partition ($TDAQ_PARTITION)")
        ("name,n",
         po::value(&m_name)->value_name("STRING")->default_value(env_string("TDAQ_APPLICATION_NAME")),
         "Name of the application ($TDAQ_APPLICATION_NAME)")
        ("database,d",
         po::value(&m_database)->value_name("STRING")->default_value(env_string("TDAQ_DB")),
         "Name of the database ($TDAQ_DB)")
        ;

    po::options_description invalidate_options("Invalidation options (only used with --cmd-line)");
    invalidate_options.add_options()
        ("run-mode",
         po::value(&m_runMode)->value_name("CHOICE"),
         "One of SoR or EoR, required if --run-number is not specified, ignored if --run-number is given")
        ("run-number",
         po::value(&m_runNumber)->value_name("NUMBER"),
         "Run number to use as a start of IOV, if not specified then run number from IS is used")
        ("lb-number",
         po::value(&m_lbNumber)->value_name("NUMBER")->default_value(0),
         "LB number to use as a start of IOV, default is 0")
        ("mu",
         po::value(&m_mu)->value_name("FLOAT")->default_value(0.0),
         "Value to use for <mu> field in the invalidated record")
        ("valid",
         po::value(&m_valid)->value_name("NUMBER")->default_value(2),
         "Value to use for Valid field in the invalidated record")
        ("is-partition",
         po::value(&m_isPartition)->value_name("STRING")->default_value(env_string("TDAQ_PARTITION")),
         "Partition name for IS server ($TDAQ_PARTITION)")
        ("is-object",
         po::value(&m_isObject)->value_name("STRING")->default_value("RunParams.RunParams"),
         "Name of the IS object to use for run number")
        ("max-run-diff",
         po::value(&m_maxRunDiff)->value_name("NUMBER")->default_value(100000),
         "Maximum allowed difference between last stored run number from COOL and new run "
         "number to be stored, use 0 to disable check")
        ;

    po::options_description cool_options("COOL database options (only used with --cmd-line)");
    cool_options.add_options()
        ("cool-db",
         po::value(&m_coolDb)->value_name("STRING")->default_value(OLC2HLT::defaultConnectionName),
         "COOL database connection string")
        ("cool-folder",
         po::value(&m_coolFolder)->value_name("STRING")->default_value(OLC2HLT::defaultFolderName),
         "COOL folder name")
        ("cool-tag",
         po::value(&m_coolTag)->value_name("STRING")->default_value(OLC2HLT::defaultTagName),
         "COOL tag name")
        ("cool-channel",
         po::value(&m_coolChannel)->value_name("NUMBER")->default_value(0U),
         "COOL channel number")
        ;

    po::options_description cmdline_options("Available options");
    cmdline_options.add_options()
        ("help,h", "Print usage and exit")
        ("cmd-line", "Use options from command line and do not try to access OKS, "
         "without this option configuration is taken from OKS and almost all "
         "command line options are ignored")
    ;
    cmdline_options.add(oks_options);
    cmdline_options.add(invalidate_options);
    cmdline_options.add(cool_options);

    // pre-parsing
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, cmdline_options), vm);

    if (vm.count("help")) {
        std::cout << "\n"
            "Invalidates OLC2HLT-stored luminosity information in COOL database"
            "\n\n"
            "Usage: " << argv[0] << " [options]\n\n"
            << cmdline_options
            << "\n\n"
            "This application invalidates luminosity information in OLC2HLT-specific COOL\n"
            "folder by creating new open-ended interval. Interval starts at LB=0 (unless\n"
            "--lb-number option provides different value) with a run number determined by\n"
            "--run-number option. If --run-number is not specified then run number is\n"
            "obtained from IS server and --run-mode must be given. If --run-mode option\n"
            "value is \"SoR\" then run number is used for IOV start, if option value is\n"
            "\"EoR\" then next run number is used for IOV start.\n"
            "\n"
            "Normally this application runs in an OLC2HLT segment and reads its configuration\n"
            "from OKS database, but one can also use it from command line by specifying\n"
            "--cmd-line option and providing values for all other options.\n"
            ;
        exit(0);
    }

    // finish parsing
    po::notify(vm);

    check_options(vm);

    return vm;
}

void Config::check_options(po::variables_map& vm)
{
    // checks options for consistency
    if (vm.count("cmd-line")) {
        // exactly one of --run-number or --run-mode must be given
        if (vm.count("run-number") == 0) {
            if (vm.count("run-mode") == 0) {
                throw errors::CommandLineError(ERS_HERE,
                        "One of --run-number or --run-mode must be specified.");
            }
        } else {
            if (vm.count("run-mode")) {
                throw errors::CommandLineError(ERS_HERE,
                        "Options --run-number and --run-mode are mutually exclusive.");
            }
        }
        if (vm.count("run-mode")) {
            auto runMode = vm["run-mode"].as<std::string>();
            if (runMode != "EoR" and runMode != "SoR" ) {
                throw errors::CommandLineError(ERS_HERE,
                        "--run-mode value must be one of SoR or EoR.");
            }
        }
    }

    // warn user if some options on command line are not used
    if (vm.count("cmd-line")) {
        const char* options[] = {"partition", "name", "database"};
        for (auto option: options) {
            if (vm.count(option) and not vm[option].defaulted()) {
                ers::warning(warnings::NotWithCmdLine(ERS_HERE, option));
            }
        }
    }
    if (vm.count("cmd-line") == 0) {
        const char* options[] = {"run-mode", "run-number", "lb-number", "mu",
                "valid", "is-partition", "is-object",
                "cool-db", "cool-folder", "cool-tag", "cool-channel"};
        for (auto option: options) {
            if (vm.count(option) and not vm[option].defaulted()) {
                ers::warning(warnings::NotWithoutCmdLine(ERS_HERE, option));
            }
        }
    }
}

void Config::read_oks_config()
{
    Configuration confdb (m_database);
    ERS_ASSERT(confdb.loaded());

    // substitute variables; needs partition object
//    const daq::core::Partition *partition_dal = daq::core::get_partition(confdb, m_partition);
//    if (partition_dal) {
//        auto sv = new daq::core::SubstituteVariables(confdb, *partition_dal);
//        confdb.register_converter(sv);
//    } else {
//        throw errors::PartitionNotFound(ERS_HERE, m_partition);
//    }

    auto conf = confdb.get<OLC2HLT::OLC2HLTInvalidateConfig>(m_name);
    if (! conf) {
        throw errors::ConfigNotFound(ERS_HERE, m_name);
    }

    // copy parameters
    m_runMode = conf->get_RunMode();
    m_runNumber = -1;
    m_lbNumber = conf->get_LBNumber();
    m_mu = conf->get_Mu();
    m_valid  = conf->get_Valid();
    m_isPartition = conf->get_ISPartition();
    if (m_isPartition.empty()) m_isPartition = env_string("TDAQ_PARTITION");
    m_isObject = conf->get_ISObject();
    m_maxRunDiff = conf->get_MaxRunDiff();

    m_coolDb = conf->get_CoolDb();
    if (m_coolDb.empty()) m_coolDb = OLC2HLT::defaultConnectionName;
    m_coolFolder = conf->get_CoolFolder();
    if (m_coolFolder.empty()) m_coolFolder = OLC2HLT::defaultFolderName;
    m_coolTag = conf->get_CoolTag();
    if (m_coolTag.empty()) m_coolTag = OLC2HLT::defaultTagName;
    m_coolChannel = conf->get_CoolChannel();
}


class Invalidator {
public:

    Invalidator(const Config& config) : m_config(config) {}

    void run();

private:

    unsigned getRunNumber();

    const Config& m_config;

};

void Invalidator::run()
{
    // get current run number
    unsigned runNumber = getRunNumber();

    // find interval that overlaps given run
    ERS_LOG("Trying to find existing COOL record: run=" << runNumber << " LBN=" << m_config.lbNumber);
    auto reader = m_config.coolReader();
    CoolReadable readable;
    bool res = reader.read(readable, runNumber, m_config.lbNumber, m_config.coolChannel);
    if (not res) {
        // this could happen either because of the problem with the database
        // or config or because there are no intervals in the folder yet.
        throw errors::IntervalNotFound(ERS_HERE, runNumber, m_config.lbNumber);
    }

    // check that interval is open
    cool::IObjectPtr iov = readable.object();
    unsigned sinceLbn = iov->since() & 0xFFFFFFFF;
    int sinceRun = iov->since() >> 32;
    unsigned untilLbn = iov->until() & 0xFFFFFFFF;
    int untilRun = iov->until() >> 32;
    ERS_LOG("Existing COOL record: from run=" << sinceRun << " LBN=" << sinceLbn
            << " to run=" << untilRun << " LBN=" << untilLbn);
    if (iov->until() != cool::ValidityKeyMax) {
        throw errors::IntervalNotOpen(ERS_HERE, sinceRun, sinceLbn, untilRun, untilLbn);
    }

    // protection against run numbers too far off
    if (m_config.maxRunDiff > 0) {
        if (runNumber - sinceRun > m_config.maxRunDiff) {
            throw errors::RunDiffTooLarge(ERS_HERE, sinceRun, runNumber);
        }
    }

    // store new thing
    ERS_LOG("Storing new COOL record: run=" << runNumber << " LBN=" << m_config.lbNumber
            << " valid=" << m_config.valid << " mu=" << m_config.mu);
    auto writer = m_config.coolWriter();
    Olc2HltWritable object(m_config.mu, m_config.valid);
    // For SoR invalidator the interval could be there already stored by EoR invalidator
    // from previous run, to avoid errors use overwrite() method instead of regular write()
    bool result = writer.overwrite(runNumber, m_config.lbNumber, object, m_config.coolChannel);
    if (not result) {
        throw errors::StoreFailed(ERS_HERE);
    }
}

unsigned Invalidator::getRunNumber()
{
    // run number from command line
    if (m_config.runNumber >= 0) {
        return static_cast<unsigned>(m_config.runNumber);
    }

    // otherwise get it from IS
    ISInfoDictionary isdict(IPCPartition(m_config.isPartition));
    RunParams run_params;
    isdict.getValue ("RunParams.RunParams", run_params);
    unsigned run = run_params.run_number;
    ERS_LOG("Fetching run number from IS: run=" << run);

    // in EoR mode get next run number
    if (m_config.runMode == "EoR") {
        ++ run;
        ERS_LOG("Increment run in EoR mode: run=" << run);
    }
    return run;
}


}

int main(int argc, char **argv)
try {

    // init IPC stuff
    IPCCore::init (argc, argv);

    Config config(argc, argv);

    Invalidator(config).run();

} catch (const po::error& exc) {
    std::cerr << exc.what() << "\nUse --help option for usage information.\n";
    return 1;
} catch (const ers::Issue& exc) {
    ers::error(exc);
    return 1;
} catch (const std::exception& ex) {
    ers::error(errors::UncaughtException(ERS_HERE, ex));
    return 1;
} catch (...) {
    ers::error(errors::UncaughtException(ERS_HERE));
    return 1;
}
