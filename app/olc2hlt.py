#!/usr/bin/env tdaq_python

"""olc2hlt application.

This script is subscribing to preferred luminosity IS data produces by OLC2COOL
and watches for changes in per-LB luminosity numbers. When the threshold is
reached it publishes new luminosity numbers in a separate IS object and
notifies CTP about new available data.

Note that this application is not in production anymore, instead
`Olc2HltControllable` class is used with `rc_pyrunner` executable.
"""

from __future__ import annotations

import argparse
import os
import sys

from pyolc2hlt import Config
from pyolc2hlt.LogHandler import initLogging

initLogging()


def main() -> int:
    """Run OLC2HLT."""
    parser = argparse.ArgumentParser(description="Re-publish preferred luminosity fro use in HLT.")
    parser.add_argument(
        "-d",
        "--database",
        dest="oks_db",
        default="",
        metavar="TDAQ_DB",
        help="Configure OLC2HLT from specified OKS database, "
        "e.g. oksconfig:/path/file.data.xml. Default is to initialize from $TDAQ_DB.",
    )
    parser.add_argument(
        "-n",
        "--name",
        dest="app_id",
        default="",
        metavar="STRING",
        help="ID of OLC2HLT application in OKS database. Default is to use " "$TDAQ_APPLICATION_NAME.",
    )
    args = parser.parse_args()

    if not args.oks_db:
        args.oks_db = os.environ.get("TDAQ_DB")
        if not args.oks_db:
            print(
                "TDAQ_DB env.var. is missing, use -d option or define env.var.",
                file=sys.stderr,
            )
            return 1

    if not args.app_id:
        args.app_id = os.environ.get("TDAQ_APPLICATION_NAME")
        if not args.app_id:
            print(
                "TDAQ_APPLICATION_NAME env.var. is missing, use -n option or define env.var.",
                file=sys.stderr,
            )
            return 1

    # read configuration
    config = Config(args.oks_db, args.app_id, "OLC2HLTApp")

    # instantiate app and run it until it stops
    app = config.makeApp()
    app.run()

    return 0


if __name__ == "__main__":
    sys.exit(main())
