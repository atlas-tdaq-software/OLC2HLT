#ifndef OLC2HLT_OLC2HLTTRANSFERRABLE_H
#define OLC2HLT_OLC2HLTTRANSFERRABLE_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "CoolUtils/IS2CoolTransferrable.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace OLC2HLT {

/// @addtogroup OLC2HLT

/**
 *  @ingroup OLC2HLT
 *
 *  Implementation of CoolUtils::IS2CoolTransferrable interface to deal with
 *  OnlPrefLumi data.
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Olc2HltTransferrable : public daq::coolutils::IS2CoolTransferrable {
public:

    // Destructor
    virtual ~Olc2HltTransferrable() {}

    // This method is implemented for being ISReadable
    virtual ISNamedInfo* create( const IPCPartition& partition, const std::string& objectName ) const override;

    // These methods are implemented for being CoolWritable
    virtual void fillRecord( cool::Record& record ) const override;
    virtual void setRecordSpec( cool::RecordSpecification& recordSpec ) const override;

private:

};

} // namespace OLC2HLT

#endif // OLC2HLT_OLC2HLTTRANSFERRABLE_H
