//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "OLC2HLT/Olc2HltCommand.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "Olc2HltTransferrable.h"
#include "CoolUtils/IS2CoolCommandHandler.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace OLC2HLT {

//----------------
// Constructors --
//----------------
Olc2HltCommand::Olc2HltCommand(const std::string& dbConnectionName,
                const std::string& dbFolderName,
                const std::string& dbTagName,
                const std::string& isObjectName)
    : daq::coolutils::IS2CoolCommand(dbConnectionName, dbFolderName, dbTagName, isObjectName)
{
}

//--------------
// Destructor --
//--------------
Olc2HltCommand::~Olc2HltCommand()
{
}

bool Olc2HltCommand::execute(unsigned int runNumber, unsigned int lumiBlockNumber)
{
    ERS_INFO("Executing NEW generic command handler for OLC2HLT IS2Cool transfer"
             " for run " << runNumber << " and lumi block " << lumiBlockNumber << " ...");

    // Create a Beam Spot object and transfer it via the handler
    Olc2HltTransferrable o2hObject;

    const bool success = handler()->transfer(runNumber, lumiBlockNumber, o2hObject);
    return success;
}

} // namespace OLC2HLT
