//----------------------------------------------------------------------
// File and Version Information:
//  $Id$
//
// Author List:
//      Andy Salnikov
//----------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "Olc2HltTransferrable.h"

//-----------------
// C/C++ Headers --
//-----------------
#include <algorithm>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "OLC2HLT/OnlPrefLumiNamed.h"
#include "CoralBase/Blob.h"
#include "CoolKernel/Record.h"
#include "ers/ers.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

ERS_DECLARE_ISSUE(
    warnings,
    EmptyBunchesArray,
    "OLC2HLT produced an empty array of per-bunch luminosities."
    " Per-bunch luminosities will be filled with 0.",
)

ERS_DECLARE_ISSUE(
    warnings,
    EmptyBunchesBadSize,
    "OLC2HLT produced per-bunch array of invalid size " << size << " (expecting 3564)."
    " Per-bunch luminosities will be truncated or filled with 0.",
    ((unsigned) size)
)

}

//      ------------------------------
//      -- Class Member Definitions --
//      ------------------------------

namespace OLC2HLT {

ISNamedInfo*
Olc2HltTransferrable::create(const IPCPartition& partition, const std::string& objectName) const
{
    return new OnlPrefLumiNamed(partition, objectName);
}

void Olc2HltTransferrable::fillRecord(cool::Record& record) const
{
    // Is there a better way to do the down cast?
    OnlPrefLumiNamed& isObject = static_cast<OnlPrefLumiNamed&>(*isNamedInfoPtr());

    record["AlgorithmID"].setValue<cool::UInt32>(isObject.AlgorithmID);
    record["LBAvInstLumi"].setValue<cool::Float>(isObject.LBAvInstLumi);
    record["LBAvEvtsPerBX"].setValue<cool::Float>(isObject.LBAvEvtsPerBX);
    record["LumiType"].setValue<cool::UInt32>(isObject.LumiType);
    record["Valid"].setValue<cool::UInt32>(isObject.Valid);

    // transfer vector data to blob, add special one-byte header,
    // note that blob expects packed floats, if IS type is different
    // from floats then we'll need to convert them
    typedef std::vector<float> bunchVector_t;
    // HLT expects exactly 3564 bunches in an array, try to protect against
    // bad input.
    if (isObject.BunchInstLumi.size() != 3564) {
        if (isObject.BunchInstLumi.empty()) {
            ers::warning(::warnings::EmptyBunchesArray(ERS_HERE));
        } else {
            ers::warning(::warnings::EmptyBunchesBadSize(ERS_HERE, isObject.BunchInstLumi.size()));
        }
        isObject.BunchInstLumi.resize(3564, 0.);
    }
    const bunchVector_t& v = isObject.BunchInstLumi;
    const int dataSize = v.size() * sizeof(bunchVector_t::value_type);
    coral::Blob blob(1 + dataSize);
    char* dst = static_cast<char*>(blob.startingAddress());

    // for meaning of these two numbers see
    // https://twiki.cern.ch/twiki/bin/view/AtlasComputing/CoolOnlineData#Folder_TDAQ_OLC_BUNCHLUMIS
    const int storageSize = 4;
    const int storageMode = 1;
    *dst++ = char(storageSize*10 + storageMode);
    const char* src = static_cast<const char*>(static_cast<const void*>((v.data())));
    std::copy(src, src+dataSize, dst);

    record["BunchInstLumi"].setValue<cool::Blob64k>(blob);
}

void Olc2HltTransferrable::setRecordSpec(cool::RecordSpecification& recordSpec) const
{
    recordSpec.extend("AlgorithmID", cool::StorageType::UInt32);
    recordSpec.extend("LBAvInstLumi", cool::StorageType::Float);
    recordSpec.extend("LBAvEvtsPerBX", cool::StorageType::Float);
    recordSpec.extend("LumiType", cool::StorageType::UInt32);
    recordSpec.extend("Valid", cool::StorageType::UInt32);
    recordSpec.extend("BunchInstLumi", cool::StorageType::Blob64k);
}

} // namespace OLC2HLT
