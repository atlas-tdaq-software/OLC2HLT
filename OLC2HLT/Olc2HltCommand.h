#ifndef OLC2HLT_OLC2HLTCOMMAND_H
#define OLC2HLT_OLC2HLTCOMMAND_H
//--------------------------------------------------------------------------
// File and Version Information:
//  $Id$
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------

//----------------------
// Base Class Headers --
//----------------------
#include "CoolUtils/IS2CoolCommand.h"

//-------------------------------
// Collaborating Class Headers --
//-------------------------------

//------------------------------------
// Collaborating Class Declarations --
//------------------------------------

//      ---------------------
//      -- Class Interface --
//      ---------------------

namespace OLC2HLT {

/// @addtogroup OLC2HLT

// This is the IS object name used for the in-run conditions updates of the HLT.
const std::string defaultObjectName     = "OLC2HLT.OnlPrefLumi";

// This is the DB connection used for the in-run conditions updates of the HLT.
const std::string defaultConnectionName = "COOLONL_TRIGGER/CONDBR2";
const std::string defaultFolderName     = "/TRIGGER/LUMI/HLTPrefLumi";
const std::string defaultTagName        = "HLTPrefLumi-HLT-UPD1-001-00";

/**
 *  @ingroup OLC2HLT
 *
 *  Implementation of IS2CoolCommand for dealing with OnlPrefLumi data
 *
 *  This software was developed for the ATLAS project.  If you use all or
 *  part of it, please give an appropriate acknowledgment.
 *
 *  @version $Id$
 *
 *  @author Andy Salnikov
 */

class Olc2HltCommand : public daq::coolutils::IS2CoolCommand {
public:

    // Constructors
    Olc2HltCommand(const std::string& dbConnectionName = defaultConnectionName,
                   const std::string& dbFolderName = defaultFolderName,
                   const std::string& dbTagName = defaultTagName,
                   const std::string& isObjectName = defaultObjectName);

    // Destructor
    virtual ~Olc2HltCommand();

    // Methods
    bool execute(unsigned int runNumber, unsigned int lumiBlockNumber);

};

} // namespace OLC2HLT

#endif // OLC2HLT_OLC2HLTCOMMAND_H
