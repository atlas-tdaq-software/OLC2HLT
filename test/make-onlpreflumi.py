#!/usr/bin/env tdaq_python

from __future__ import annotations

import argparse
import sys

from coldpie import cool, coral

# Fodler names and their corresponding description and versioning mode
onlpreflumi = (
    "/TRIGGER/LUMI/OnlPrefLumi",
    "Online Preferred Luminosity",
    cool.FolderVersioning.SINGLE_VERSION,
)
hltpreflumi = ("/TRIGGER/LUMI/HLTPrefLumi", "HLT Preferred Luminosity", cool.FolderVersioning.MULTI_VERSION)

# column definition
schema = [
    ("AlgorithmID", cool.StorageType.UInt32),
    ("LBAvInstLumi", cool.StorageType.Float),
    ("LBAvEvtsPerBX", cool.StorageType.Float),
    ("LumiType", cool.StorageType.UInt32),
    ("Valid", cool.StorageType.UInt32),
    ("BunchInstLumi", cool.StorageType.Blob64k),
]


def main() -> int:
    """Create COOL folder for OnlPrefLumi"""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-o",
        "--onlpreflumi",
        default=False,
        action="store_true",
        help="Create SINGLE_VERSION /TRIGGER/LUMI/OnlPrefLumi, by default "
        "it creates MULTI_VERSION /TRIGGER/LUMI/HLTPrefLumi",
    )
    parser.add_argument(
        "-r",
        "--record",
        default=False,
        action="store_true",
        help="Create single record from 0 to +Inf, only makes sense for " " MULTI_VERSION folders",
    )
    parser.add_argument(
        "-t", "--tag", default="HLTPrefLumi-HLT-UPD1-001-00", help="Tag name for new record, def: %(default)s"
    )
    parser.add_argument("-l", "--lock", default=False, action="store_true", help="Lock the tag")
    parser.add_argument(
        "connection",
        help="database connection string, e.g. " "sqlite://;schema=preflumi.sqlite;dbname=CONDBR2",
    )
    args = parser.parse_args()

    # open database (or create if sqlite)
    dbspec = args.connection
    dbSvc = cool.DatabaseSvcFactory.databaseService()
    try:
        db = dbSvc.openDatabase(dbspec, False)
    except Exception:
        if dbspec.startswith("sqlite"):
            print("Creating new database")
            db = dbSvc.createDatabase(dbspec)
        else:
            print("Unable to create database", dbspec, file=sys.stderr)
            return 1

    folderName, desc, fversion = onlpreflumi if args.onlpreflumi else hltpreflumi
    # define Folder spec
    rspec = cool.RecordSpecification([cool.FieldSpecification(col, type) for col, type in schema])
    fspec = cool.FolderSpecification(fversion, rspec)

    # make folder
    print("Creating fodler", folderName)
    folder = db.createFolder(folderName, fspec, desc, True)

    # make channel 0
    print("Creating channel 0 for folder", folderName)
    folder.createChannel(0, "ATLAS_PREFERRED", "Preferred luminosity values, calculated by OLC2COOL")

    if args.record:
        # write single record
        record = cool.Record(rspec)
        record["AlgorithmID"] = 0
        record["LBAvInstLumi"] = 0
        record["LBAvEvtsPerBX"] = 0
        record["LumiType"] = 0
        record["Valid"] = 2
        record["BunchInstLumi"] = coral.Blob()
        print("Creating initial record with tag", args.tag)
        folder.storeObject(cool.ValidityKeyMin, cool.ValidityKeyMax, record, 0, args.tag, True)
        if args.lock:
            print("Locking tag", args.tag)
            folder.setTagLockStatus(args.tag, 1)

    return 0


if __name__ == "__main__":
    sys.exit(main())
