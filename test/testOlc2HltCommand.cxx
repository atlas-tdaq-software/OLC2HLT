//
// $Id: dump_config.cxx 112519 2013-04-18 06:52:40Z salnikov $
//

//-----------------
// C/C++ Headers --
//-----------------
#include <string>
#include <iostream>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "ers/ers.h"
#include "OLC2HLT/Olc2HltCommand.h"
#include "cmdl/cmdargs.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

namespace {

// by default use sqlite, not oracle
const std::string dbConnection = "sqlite://;schema=olc2hlt_test.db;dbname=CONDBR2";
const std::string dbFolder = OLC2HLT::defaultFolderName;
const std::string dbTag = OLC2HLT::defaultTagName;
const std::string isObject = OLC2HLT::defaultObjectName;

}

int main(int argc, char **argv)
{
    // command line
    CmdLine cmdl(argv[0]);

    CmdArgUsage usage_arg('h', "help", "print usage and exit");
    cmdl << usage_arg;

    // database
    const std::string descr1 = "connection string for COOL database (default: " + ::dbConnection + ")";
    CmdArgStr conn_str_arg('d', "database", "conn_str", descr1.c_str());
    conn_str_arg = ::dbConnection.c_str();
    cmdl << conn_str_arg;

    // folder name
    const std::string descr2 = "folder name for COOL (default: " + ::dbFolder + ")";
    CmdArgStr folder_arg('f', "folder", "path", descr2.c_str());
    folder_arg = ::dbFolder.c_str();
    cmdl << folder_arg;

    // tag name
    const std::string descr3 = "COOL tag name (default: " + ::dbTag + ")";
    CmdArgStr tag_arg('t', "tag", "string", descr3.c_str());
    tag_arg = ::dbTag.c_str();
    cmdl << tag_arg;

    // tag name
    const std::string descr4 = "IS object name, including server name (default: " + ::isObject + ")";
    CmdArgStr isobj_arg('i', "is-object", "string", descr4.c_str());
    isobj_arg = ::isObject.c_str();
    cmdl << isobj_arg;

    // run number
    CmdArgInt run_arg('r', "run", "number", "run number (default: 0)");
    run_arg = 0;
    cmdl << run_arg;

    // LB number
    CmdArgInt lbn_arg('l', "lbn", "number", "LB number (default: 0)");
    lbn_arg = 0;
    cmdl << lbn_arg;

    /* parse the cmd-line parameters */
    CmdArgvIter arg_iter(argc - 1, argv + 1);
    cmdl.description ("Test application for Olc2HltCommand");
    cmdl.parse (arg_iter);

    const std::string dbConnection = static_cast<const char *>(conn_str_arg);
    const std::string dbFolder = static_cast<const char *>(folder_arg);
    const std::string dbTag = static_cast<const char *>(tag_arg);
    const std::string isObject = static_cast<const char *>(isobj_arg);
    const int run = run_arg;
    const int lbn = lbn_arg;

    try {
        // daq::beamspot::BeamSpotIS2CoolCommand command( dbConnection, dbFolder, dbTag, isObject );
        OLC2HLT::Olc2HltCommand command(dbConnection, dbFolder, dbTag, isObject);
        bool success = command.execute(run, lbn);
        if (not success) {
          return 1;
        }
    } catch (std::exception& e) {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 2;
    }

    return 0;
}
