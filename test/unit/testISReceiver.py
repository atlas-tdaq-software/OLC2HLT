#!/usr/bin/env tdaq_python

"""Unit test for StableBeamsFlag class."""

from __future__ import annotations

import queue
import unittest
from collections.abc import Iterable

from _mocks import MockISDynAny, MockISInfoReceiver, MockOnlPrefLumi
from ispy import Reason
from pyolc2hlt import ISReceiver


class TestCase(unittest.TestCase):
    """Test case for ISRecevier."""

    def setUp(self) -> None:
        ISReceiver.ISInfoReceiverClass = MockISInfoReceiver
        ISReceiver.ISInfoDynAnyClass = MockISDynAny

        self.part = "part"
        self.infoName = "OLC.OnlPrefLumi"

        self._run = 222222
        self.lbn = 0
        self.time = 1500000000 * 1000000000

        self.queue: queue.Queue = queue.Queue()

    def _makeISInfo(
        self,
        lumi: float,
        lb_len: int = 60,
        channel: int = 0,
        algoID: int = 0,
        lumiType: int = 0,
        valid: int = 0,
    ) -> MockOnlPrefLumi:
        self.lbn += 1
        endTime = self.time + lb_len * 1000000000
        per_bunch_lumi = lumi / 5.0
        info = MockOnlPrefLumi(
            RunNumber=self._run,
            LBN=self.lbn,
            StartTime=self.time,
            EndTime=endTime,
            Channel=channel,
            AlgorithmID=algoID,
            LBAvInstLumi=lumi,
            LBAvEvtsPerBX=per_bunch_lumi,
            LumiType=lumiType,
            Valid=valid,
            BunchInstLumi=[
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
            ],
        )
        self.time = endTime
        return info

    def _publish(self, data: Iterable) -> None:
        for entry in data:
            self.queue.put(entry)
        self.queue.join()

    def test01(self) -> None:
        recv = ISReceiver(self.part, self.infoName, self.queue)
        recv.subscribe()

        mock_is_recv = recv.receiver
        assert mock_is_recv is not None

        isinfo = self._makeISInfo(1.0)
        mock_is_recv.receive(self.infoName, isinfo, isinfo.EndTime + 1000000, reason=Reason.Subscribed)
        self.assertEqual(self.queue.qsize(), 1)
        reason, isinfo2 = self.queue.get()
        self.assertEqual(reason, Reason.Subscribed)
        self.assertEqual(isinfo2.RunNumber, self._run)
        self.assertEqual(isinfo2.LBN, self.lbn)
        self.assertEqual(isinfo2.LBAvInstLumi, 1.0)

        isinfo = self._makeISInfo(100.0)
        mock_is_recv.receive(self.infoName, isinfo, isinfo.EndTime + 1000000, reason=Reason.Updated)
        self.assertEqual(self.queue.qsize(), 1)
        reason, isinfo2 = self.queue.get()
        self.assertEqual(reason, Reason.Updated)

        recv.unsubscribe()


if __name__ == "__main__":
    unittest.main()
