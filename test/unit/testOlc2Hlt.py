#!/usr/bin/env tdaq_python

"""Unit test for StableBeamsFlag class."""

from __future__ import annotations

import queue
import unittest
import unittest.mock
from collections.abc import Iterable, Iterator
from contextlib import contextmanager

import cool_utils
from _mocks import MockISDynAny, MockISInfoReceiver, MockISObjectStableBeams, MockOnlPrefLumi
from ispy import Reason
from pyolc2hlt import Decider, ISReceiver, Olc2Hlt, StableBeamsFlag, StableDecider, ThresholdDecider


@contextmanager
def runOlc2Hlt(olc2hlt: Olc2Hlt) -> Iterator[Olc2Hlt]:
    """Stop Olc2Hlt even if test generates exception"""
    olc2hlt.start()
    try:
        yield olc2hlt
    except Exception:
        raise
    finally:
        olc2hlt.stop()


class MockExec:
    """Callable for mocking the execution."""

    def __init__(self) -> None:
        self.args: object = None

    def __call__(self, args: object, **kw: object) -> str:
        self.args = args
        return "OK"


class MockISObject:
    """Mock class for ISObject."""

    checked_in: list[MockISObject] = []

    def __init__(self, partName: str, infoName: str, infoType: str) -> None:
        self.partName = partName
        self.infoName = infoName
        self.infoType = infoType
        self.LBN = 0
        self.LBAvInstLumi = -1.0

    def checkin(self) -> None:
        MockISObject.checked_in.append(self)


class TestCase(unittest.TestCase):
    """Test cases for Olc2Hlt class."""

    def setUp(self) -> None:
        Olc2Hlt.ISObjectClass = MockISObject
        StableBeamsFlag.ISObjectClass = MockISObjectStableBeams
        ISReceiver.ISInfoReceiverClass = MockISInfoReceiver
        ISReceiver.ISInfoDynAnyClass = MockISDynAny

        lumiChangePercent = 10.0
        lumiChangePercentUnstable = 25.0
        self.unstableDeadBand = 0.01
        self.minLBLength = 10
        sbeamFlag = StableBeamsFlag("initial", "LHC.StableBeamsFlag")
        stableDecider = StableDecider(lumiChangePercent)
        unstableDecider = ThresholdDecider(self.unstableDeadBand, lumiChangePercentUnstable)
        self.decider = Decider(stableDecider, unstableDecider, self.minLBLength, sbeamFlag)

        self.infoName = "OLC2COOL.OnlPrefLumi"
        self._run = 222222
        self.lbn = 0
        self.time = 1500000000 * 1000000000
        self.queue: queue.Queue = queue.Queue()

    def _makeISInfo(
        self,
        lumi: float,
        lb_len: int = 60,
        channel: int = 0,
        algoID: int = 0,
        lumiType: int = 0,
        valid: int = 0,
    ) -> MockOnlPrefLumi:
        self.lbn += 1
        endTime = self.time + lb_len * 1000000000
        per_bunch_lumi = lumi / 5.0
        info = MockOnlPrefLumi(
            RunNumber=self._run,
            LBN=self.lbn,
            StartTime=self.time,
            EndTime=endTime,
            Channel=channel,
            AlgorithmID=algoID,
            LBAvInstLumi=lumi,
            LBAvEvtsPerBX=per_bunch_lumi,
            LumiType=lumiType,
            Valid=valid,
            BunchInstLumi=[
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
                0.0,
                per_bunch_lumi,
            ],
        )
        self.time = endTime
        return info

    def _publish(self, data: Iterable[tuple[Reason, MockOnlPrefLumi]]) -> None:
        assert self.mock_is_recv is not None
        for reason, isinfo in data:
            self.mock_is_recv.receive(self.infoName, isinfo, isinfo.EndTime + 1000000, reason=reason)
        # synchronize with processing
        self.queue.join()

    def test01(self) -> None:
        dstPart, dstInfo = "", "OLC2HLT.OnlPrefLumi"
        noCTPUpdate = False
        recv = ISReceiver("OLC", self.infoName, self.queue)
        olc2hlt = Olc2Hlt(self.queue, dstPart, dstInfo, self.decider, noCTPUpdate, recv, ctp_partition="?")

        with runOlc2Hlt(olc2hlt):
            # recv.receiver is only defined after subscribe()
            self.mock_is_recv = recv.receiver

            olc2hlt._ctp_commander = unittest.mock.MagicMock()

            #  tuples (stableBeams, reason, lumiValue, expectQueueSize,
            #          expectValue, expectLBN, kw)
            updates: list[tuple[bool, Reason, float, int, float, int, dict]] = [
                (False, Reason.Subscribed, 0.001, 1, 0.001, 1, {}),
                # 0 is consistent with 0.001
                (False, Reason.Updated, 0.0, 1, 0.001, 1, {}),
                # LB is too short, so no update
                (
                    False,
                    Reason.Updated,
                    0.1,
                    1,
                    0.001,
                    1,
                    {"lb_len": self.minLBLength / 2},
                ),
                # jump above thresold, still non-stable beams
                (False, Reason.Updated, 0.1, 2, 0.1, 4, {}),
                # relative change 24%, no update
                (False, Reason.Updated, 0.124, 2, 0.1, 4, {}),
                # relative change 26%, will update
                (False, Reason.Updated, 0.126, 3, 0.126, 6, {}),
                # drop to almost zero
                (False, Reason.Updated, 0.001, 4, 0.001, 7, {}),
                # jump again
                (False, Reason.Updated, 100.0, 5, 100.0, 8, {}),
                # go to stable beams
                # small change but should trigger update
                (True, Reason.Updated, 101.0, 6, 101.0, 9, {}),
                # small change, no update
                (True, Reason.Updated, 110.0, 6, 101.0, 9, {}),
                # >10% change
                (True, Reason.Updated, 112.0, 7, 112.0, 11, {}),
                # <10% drop, no update
                (True, Reason.Updated, 101.0, 7, 112.0, 11, {}),
                # >10% drop, should update
                (True, Reason.Updated, 100.0, 8, 100.0, 13, {}),
                # Drop to 0
                (True, Reason.Updated, 0.0, 9, 0.0, 14, {}),
                # Any change from 0 should update
                (True, Reason.Updated, 1e-10, 10, 1e-10, 15, {}),
                # back to non-stable, below threshold
                (False, Reason.Updated, 0.001, 10, 1e-10, 15, {}),
                # Info object is deleted, no updates
                (False, Reason.Deleted, 100.0, 10, 1e-10, 15, {}),
                # Info object is re-created
                (False, Reason.Created, 100.0, 11, 100.0, 18, {}),
            ]

            self.assertEqual(len(MockISObject.checked_in), 0)

            for (
                stableBeams,
                reason,
                lumiValue,
                expectQueueSize,
                expectValue,
                expectLBN,
                kw,
            ) in updates:
                MockISObjectStableBeams.stableBeams = stableBeams
                self._publish([(reason, self._makeISInfo(lumiValue, **kw))])
                self.assertEqual(len(MockISObject.checked_in), expectQueueSize)
                info = MockISObject.checked_in[-1]
                self.assertEqual(info.LBN, expectLBN)
                self.assertEqual(info.LBAvInstLumi, expectValue)

            self.assertEqual(olc2hlt._ctp_commander.update.call_count, 11)
            olc2hlt._ctp_commander.update.assert_called_with(cool_utils.FolderIndex.LUMINOSITY)


if __name__ == "__main__":
    unittest.main()
