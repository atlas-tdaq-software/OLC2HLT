#!/usr/bin/env tdaq_python

"""Unit test for Olc2HltInvalidator class.
"""

from __future__ import annotations

import unittest

from pyolc2hlt import Olc2HltInvalidator


class TestCase(unittest.TestCase):
    """Unit test fot invalidator."""

    def test01(self) -> None:
        uid = "olc2hlt-sor-invalidate"
        inval = Olc2HltInvalidator(uid, testing=True)
        inval.run()
        assert inval.exec_result is not None
        self.assertEqual(inval.exec_result.args, ["/usr/bin/echo", "olc2hlt-invalidate", "-n", uid])


if __name__ == "__main__":
    unittest.main()
