#!/usr/bin/env tdaq_python

"""Unit test for StableBeamsFlag class.
"""

from __future__ import annotations

import unittest

from _mocks import MockISObjectStableBeams
from pyolc2hlt import StableBeamsFlag


class TestCase(unittest.TestCase):
    """Unit test for StableBeamFlag."""

    def setUp(self) -> None:
        StableBeamsFlag.ISObjectClass = MockISObjectStableBeams

    def test01(self) -> None:
        # this will cause exception which returns False
        sbeams = StableBeamsFlag("initial", "LHC.StableBeamsFlag")
        MockISObjectStableBeams.stableBeams = None
        self.assertFalse(sbeams())

        # no exceptions, plain False
        MockISObjectStableBeams.stableBeams = False
        self.assertFalse(sbeams())

        # Stable now
        MockISObjectStableBeams.stableBeams = True
        self.assertTrue(sbeams())


if __name__ == "__main__":
    unittest.main()
