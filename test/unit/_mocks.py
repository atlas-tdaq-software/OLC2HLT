"""Mock classes for use in unit tests."""

from __future__ import annotations

import dataclasses
from collections.abc import Callable, Sequence
from typing import TYPE_CHECKING

import ers
from ispy import Reason

if TYPE_CHECKING:
    from ispy import IPCPartition


@dataclasses.dataclass
class MockOnlPrefLumi:
    """Python mock for IS OnlPrefLumi structure."""

    LBAvInstLumi: float = 0.0
    StartTime: int = 0
    EndTime: int = 0
    RunNumber: int = 0
    LBN: int = 0
    Channel: int = 0
    AlgorithmID: int = 0
    LBAvEvtsPerBX: float = 0.0
    LumiType: int = 0
    Valid: int = 0
    BunchInstLumi: Sequence[float] = dataclasses.field(default_factory=list)


class MockISCallbackInfo:
    def __init__(self, infoName: str, reason: Reason, isinfo: str, istype: str, time: float, tag: int = 0):
        self._infoName = infoName
        self._reason = reason
        self._isinfo = isinfo
        self._istype = istype
        self._time = time
        self._tag = tag

    def name(self) -> str:
        return self._infoName

    def reason(self) -> Reason:
        return self._reason

    def type(self) -> str:
        return self._istype

    def time(self) -> float:
        return self._time

    def tag(self) -> int:
        return self._tag

    def value(self, mocDynAny: MockISDynAny) -> None:
        mocDynAny.__dict__.update(self._isinfo.__dict__)


class MockISDynAny:
    pass


class MockISInfoReceiver:
    def __init__(self, part: IPCPartition, serialize_callbacks: bool = False):
        self.subscriptions: dict[str, tuple[Callable[[MockISCallbackInfo], None], set[Reason]]] = {}

    def scheduleSubscription_info(
        self, infoName: str, callback: Callable[[MockISCallbackInfo], None], event_mask: list[Reason]
    ) -> None:
        self.subscriptions[infoName] = (callback, set(event_mask))

    def unsubscribe(self, infoName: str, wait: bool) -> None:
        del self.subscriptions[infoName]

    def receive(
        self,
        infoName: str,
        isinfo: str,
        time: float,
        reason: Reason = Reason.Updated,
        istype: str = "OnlPrefLumi",
        tag: int = 0,
    ) -> None:
        callback, event_mask = self.subscriptions[infoName]
        if reason in event_mask:
            cinfo = MockISCallbackInfo(infoName, reason, isinfo, istype, time, tag)
            callback(cinfo)


class MockISObjectStableBeams:
    stableBeams: bool | None = None

    def __init__(self, sbeamPart: str, sbeamInfo: str, infoType: str):
        self.value: bool | None = None

    def checkout(self) -> None:
        if self.stableBeams is None:
            raise ers.Issue("mocking failure", {}, None)
        self.value = self.stableBeams
