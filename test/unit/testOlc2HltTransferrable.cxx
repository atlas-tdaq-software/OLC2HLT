// Unit tests for Olc2HltTransferrable classe

#include "../../src/Olc2HltTransferrable.h"
#include "OLC2HLT/OnlPrefLumiNamed.h"

#include "CoolKernel/Record.h"
#include "CoolKernel/RecordSpecification.h"

#define BOOST_TEST_MODULE olc2hlt_unit_Olc2HltTransferrable
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>
#include <boost/mpl/vector.hpp>


namespace {

// need to override create() method to make a test IS object
class TestOlc2HltTransferrable : public OLC2HLT::Olc2HltTransferrable {
public:
    TestOlc2HltTransferrable(int nbunches=3564) : n_bunches(nbunches) {}

    virtual ISNamedInfo* create(const IPCPartition& partition, const std::string& objectName) const override {
        auto is_obj = new OLC2HLT::OnlPrefLumiNamed(partition, objectName);
        is_obj->BunchInstLumi.resize(n_bunches, -1.);
        return is_obj;
    }

private:

    int n_bunches;
};

} // namespace

BOOST_AUTO_TEST_CASE(test_nbunches)
{
    IPCCore::init({});

    IPCPartition partition("");
    std::string objectName("server.name");

    for (int nbunches: {3564, 0, 1, 10000}) {
        TestOlc2HltTransferrable trans(nbunches);
        trans.initialize(partition, objectName);

        cool::RecordSpecification recordSpec;
        trans.setRecordSpec(recordSpec);
        cool::Record record(recordSpec);
        trans.fillRecord(record);

        auto& blobField = record["BunchInstLumi"];
        BOOST_TEST(not blobField.isNull());
        auto& blob = blobField.data<cool::Blob64k>();
        BOOST_TEST(blob.size() == 14257);
    }

}
