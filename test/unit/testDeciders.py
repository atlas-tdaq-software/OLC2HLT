#!/usr/bin/env tdaq_python

"""Unit test for Smoother class.
"""

from __future__ import annotations

import logging
import unittest
from collections.abc import Iterable
from typing import TYPE_CHECKING

from _mocks import MockOnlPrefLumi
from pyolc2hlt import SmootherDecider, StableDecider, ThresholdDecider

if TYPE_CHECKING:
    from pyolc2hlt._types import _Decider


logging.basicConfig(level=logging.WARNING)


class TestSmoother(unittest.TestCase):
    """Unit tests for decider classes."""

    def _testDecider(self, decider: _Decider, data: Iterable[tuple[float, float, bool]]) -> None:
        """Check decision.

        `data` is ia sequence of (old, new, expected) tuples.
        """
        t0 = 0
        for lumi1, lumi2, expected in data:
            t1 = int(t0 + 60e9)
            t2 = int(t1 + 60e9)
            oldData = MockOnlPrefLumi(LBAvInstLumi=lumi1, StartTime=t0, EndTime=t1)
            newData = MockOnlPrefLumi(LBAvInstLumi=lumi2, StartTime=t1, EndTime=t2)
            self.assertEqual(decider.decide(oldData, newData), expected)
            t0 = t1

    def _testSmoother(self, decider: SmootherDecider, data: Iterable[tuple[float, bool, float]]) -> None:
        """Check smoother decision.

        `data` is ia sequence of (lumi, expected, value) tuples, in first tuple
        expected is not used.
        """
        t0 = 0
        for i, (lumi, expected, value) in enumerate(data):
            if i == 0:
                oldLumi = lumi
                continue
            t1 = int(t0 + 60e9)
            t2 = int(t1 + 60e9)
            oldData = MockOnlPrefLumi(LBAvInstLumi=oldLumi, StartTime=t0, EndTime=t1)
            newData = MockOnlPrefLumi(LBAvInstLumi=lumi, StartTime=t1, EndTime=t2)
            decision = decider.decide(oldData, newData)
            # print oldData, newData, decision, decider.value
            self.assertEqual(decision, expected)
            self.assertEqual(decider.value, value)
            if decision:
                oldLumi = lumi
            t0 = t1

    def _testThreshold(self, decider: _Decider, data: Iterable[tuple[float, bool]]) -> None:
        """Check decision.

        data is ia sequence of (lumi, expected) tuples, in first tuple expected
        is not used.
        """
        t0 = 0
        for i, (lumi, expected) in enumerate(data):
            if i == 0:
                oldLumi = lumi
                continue
            t1 = int(t0 + 60e9)
            t2 = int(t1 + 60e9)
            oldData = MockOnlPrefLumi(LBAvInstLumi=oldLumi, StartTime=t0, EndTime=t1)
            newData = MockOnlPrefLumi(LBAvInstLumi=lumi, StartTime=t1, EndTime=t2)
            decision = decider.decide(oldData, newData)
            # -- print oldData, newData, decision
            self.assertEqual(decision, expected)
            if decision:
                oldLumi = lumi
            t0 = t1

    def testStableDecider(self) -> None:
        """Test StableDecider"""
        # (old, new, expected)
        lumis = [
            (0.0, 1.0, True),  # significant increase
            (0.0, 1e-30, True),  # significant increase
            (0.0, 1e30, True),  # significant increase
            (1.0, 1.0, False),  # no change
            (1.0, 1.099, False),  # increase slightly below 10%
            (1.0, 1.101, True),  # increase slightly above 10%
            (1.0, 0.901, False),  # decrease slightly below 10%
            (1.0, 0.899, True),  # decrease slightly above 10%
            (1.0, 0.0, True),
        ]  # significant drop

        decider = StableDecider(10)

        self._testDecider(decider, lumis)

    def testSmootherDecider(self) -> None:
        """Test SmootherDecider"""
        decider = SmootherDecider(10, 1000, True, 10)

        # suppress single peak
        #       (lumi, expected)
        lumis = [
            (0.0, False, 0.0),
            (0.0, False, 0.0),
            (0.1, False, 0.0),  # peak is ignored
            (0.0, False, 0.0),
            (0.0, False, 0.0),
            (0.0, False, 0.0),
        ]

        self._testSmoother(decider, lumis)

        decider.reset()

        #       (lumi, expected)
        lumis = [
            (0.0, False, 0.0),
            (0.0, False, 0.0),
            (1.0, False, 0.0),  # suppressed as a peak
            (1.0, True, 2.0 / 3.0),  # 2 non-zero LBs out of 3
            (0.0, True, 2.0 / 4.0),  # significant change 0.66 -> 0.5 = -25%
            (0.0, False, 2.0 / 5.0),  # no change because raw value does not
            (0.0, False, 2.0 / 6.0),
        ]  # same thing

        self._testSmoother(decider, lumis)

        decider.reset()

        #       (lumi, expected)
        lumis = [
            (0.0, False, 0.0),
            (1.0, True, 1.0),  # this is not a spike
            (1.0, False, 1.0),  # raw value does not change
            (1.0, False, 1.0),
            (1.0, False, 1.0),
            (0.0, True, 0.8),  # significant change
            (0.0, False, 4.0 / 6.0),  # raw value does not change
            (0.0, False, 4.0 / 7.0),
        ]  # raw value does not change

        self._testSmoother(decider, lumis)

    def testThresholdDecider01(self) -> None:
        """Test ThresholdDecider, all values below threshold values."""
        decider = ThresholdDecider(0.01, 30)

        #       (lumi, expected)
        lumis = [
            (0.0, False),
            (0.001, False),
            (0.005, False),
            (0.000008, False),
            (0.0099, False),
            (0.0, False),
        ]

        self._testThreshold(decider, lumis)

    def testThresholdDecider02(self) -> None:
        """Test ThresholdDecider, switch from 0 to to a value and back."""
        decider = ThresholdDecider(0.01, 30)

        #       (lumi, expected)
        lumis = [
            (0.0, False),
            (0.001, False),  # below threshold
            (0.0101, True),  # above threshold
            (0.011, False),  # change is too small
            (0.001, True),  # go back below threshold
            (0.0, False),
        ]  # stay below

        self._testThreshold(decider, lumis)

    def testThresholdDecider03(self) -> None:
        """Test ThresholdDecider, checking relative changes"""
        decider = ThresholdDecider(0.01, 30)

        #       (lumi, expected)
        lumis = [
            (0.0, False),
            (0.001, False),  # below threshold
            (0.0101, True),  # now above threshold
            (0.0143, True),  # significant increase
            (0.016, False),  # insignificant increase
            (0.01001, True),  # significant reduction
            (0.1, True),  # significant increase
            (0.001, True),  # drop below threshold
            (0.0, False),
        ]  # stays below

        self._testThreshold(decider, lumis)


if __name__ == "__main__":
    unittest.main()
