#!/usr/bin/env tdaq_python

"""Unit test for Smoother class.
"""

from __future__ import annotations

import unittest

from pyolc2hlt import Smoother


class TestSmoother(unittest.TestCase):
    """Unit tests for Smoother class."""

    def test01(self) -> None:
        """Test for max 3 LBs, no spike removal."""
        smoother = Smoother(3, 10000000, False)

        smoother.addLB(0, 1, 10.0)
        self.assertEqual(len(smoother.lbs), 1)
        self.assertEqual(smoother.value(), 10.0)

        smoother.addLB(2, 3, 20.0)
        self.assertEqual(len(smoother.lbs), 2)
        self.assertEqual(smoother.value(), 15.0)

        smoother.addLB(3, 6, 20.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 18.0)

        smoother.addLB(6, 7, 0.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 16.0)

        smoother.addLB(7, 8, 0.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 12.0)

        smoother.addLB(8, 9, 0.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 0.0)

        smoother.reset()
        self.assertEqual(len(smoother.lbs), 0)
        self.assertEqual(smoother.value(), 0.0)

    def test02(self) -> None:
        """Test for time limits."""
        smoother = Smoother(1000, 10, False)

        smoother.addLB(0000000000, 2000000000, 10.0)
        self.assertEqual(len(smoother.lbs), 1)
        self.assertEqual(smoother.value(), 10.0)

        smoother.addLB(2000000000, 4000000000, 20.0)
        self.assertEqual(len(smoother.lbs), 2)
        self.assertEqual(smoother.value(), 15.0)

        smoother.addLB(5000000000, 7000000000, 30.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 20.0)

        smoother.addLB(8000000000, 12000000000, 15.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 20.0)

        smoother.addLB(12000000000, 16000000000, 5.0)
        self.assertEqual(len(smoother.lbs), 2)
        self.assertEqual(smoother.value(), 10.0)

    def test03(self) -> None:
        """Test for spikes removal."""
        smoother = Smoother(5, 10, True)

        smoother.addLB(0, 1, 0.0)
        self.assertEqual(len(smoother.lbs), 1)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 1.0)
        self.assertEqual(len(smoother.lbs), 2)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 0.0)
        self.assertEqual(len(smoother.lbs), 3)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 0.0)
        self.assertEqual(len(smoother.lbs), 4)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 0.0)
        self.assertEqual(len(smoother.lbs), 5)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 0.0)
        self.assertEqual(len(smoother.lbs), 5)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 1.0)
        self.assertEqual(len(smoother.lbs), 5)
        self.assertEqual(smoother.value(), 0.0)

        smoother.addLB(0, 1, 4.0)
        self.assertEqual(len(smoother.lbs), 5)
        self.assertEqual(smoother.value(), 1.0)


if __name__ == "__main__":
    unittest.main()
