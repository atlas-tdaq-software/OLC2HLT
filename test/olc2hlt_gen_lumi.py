#!/bin/env tdaq_python

"""olc2hlt_gen_lumi application for testing olc2hlt.

This script generates some fake data to be consumed by olc2hlt.
"""

from __future__ import annotations

import argparse
import logging
import math
import os
import queue
import sys
import time
from collections.abc import Callable, Iterator

import ispy

_LOG = logging.getLogger("olc2hlt_gen_lumi")


class _FakeLBGen:
    """
    Generator for LB data, makes totally fake data.
    """

    def __init__(self, run: int, startLBN: int, lbLengthSec: float):
        self.run = run
        self.lbn = startLBN
        self.lbLengthSec = lbLengthSec

    def __call__(self) -> Iterator[tuple[int, int, int, int]]:
        """Return tuple (run, lbn, t0, t1)."""
        t1 = int((time.time() - 3 * self.lbLengthSec) * 1e9)
        while True:
            # set arbitrary time for LB, we are usually 3 LBS behind
            t0 = t1
            t1 = int((time.time() - 2 * self.lbLengthSec) * 1e9)

            yield self.run, self.lbn, t0, t1

            # sleep until next lb
            time.sleep(self.lbLengthSec)

            self.lbn += 1


class _LBGenIS:
    """Generator for LB data, reads current LB values from standard RunParams
    IS server.
    """

    def __init__(self, partition: str, isinfo: str = "RunParams.LumiBlock"):
        self.queue: queue.Queue = queue.Queue()

        # subscribe to IS data
        part = ispy.IPCPartition(partition)
        self.receiver = ispy.ISInfoReceiver(part, True)
        event_mask = [
            ispy.Reason.Created,
            ispy.Reason.Updated,
            ispy.Reason.Subscribed,
            ispy.Reason.Deleted,
        ]
        self.receiver.scheduleSubscription_info(isinfo, self._callback, event_mask)

    def _callback(self, cinfo: ispy.ISInfo) -> None:
        """Handle subscription to single info."""
        _LOG.debug(
            "ISInfo: %s  reason: %s  type: %s  time: %s",
            cinfo.name(),
            cinfo.reason(),
            cinfo.type(),
            cinfo.time(),
        )

        if cinfo.reason() in [
            ispy.Reason.Created,
            ispy.Reason.Updated,
            ispy.Reason.Subscribed,
        ]:
            val = ispy.ISInfoDynAny()
            cinfo.value(val)
            # put it into a queue
            self.queue.put(val)

    def __call__(self) -> Iterator[tuple[int, int, int, int]]:
        """Return tuple (run, lbn, t0, t1)."""
        queue = []  # short queue of up to 3 last LBs

        while True:
            val = self.queue.get(True)

            run = val.RunNumber
            lbn = val.LumiBlockNumber
            time_ns = val.Time

            queue.append((run, lbn, time_ns))

            # OLC data is delayed, simulate it here as well
            if len(queue) > 2:
                run, lbn, t0 = queue[0]
                del queue[0]
                t1 = queue[0][2]

                yield run, lbn, t0, t1


# ------------------------
# Exported definitions --
# ------------------------


def main() -> int:
    """Run the whole thing."""
    def_part = os.environ.get("$TDAQ_PARTITION", "ATLAS")

    parser = argparse.ArgumentParser(description="Re-publish preferred luminosity fro use in HLT.")
    parser.add_argument(
        "-p",
        "--partition",
        default=def_part,
        metavar="STRING",
        help="Name of partition containing output IS server, def: %(default)s",
    )
    parser.add_argument(
        "-s",
        "--server",
        default="OLC2COOL-OUT",
        metavar="STRING",
        help="Output IS server name, def: %(default)s",
    )
    parser.add_argument(
        "-o",
        "--object",
        dest="obj_name",
        default="OnlPrefLumi",
        metavar="STRING",
        help="Output IS object name, def: %(default)s",
    )
    parser.add_argument(
        "-v",
        "--luminosity",
        default=1000,
        metavar="NUMBER",
        type=float,
        help="Starting luminosity value, def: %(default)s",
    )
    parser.add_argument(
        "-m",
        "--pileup",
        default=40,
        metavar="NUMBER",
        type=float,
        help="Starting <mu> value, def: %(default)s",
    )
    parser.add_argument(
        "-t",
        "--mean-lifetime",
        default=3600.0,
        metavar="SECONDS",
        type=float,
        help="Mean lifetime of the luminosity in seconds, def: %(default)s",
    )
    parser.add_argument(
        "-1",
        "--once",
        default=False,
        action="store_true",
        help="Update IS once and exit",
    )
    parser.add_argument(
        "-L",
        "--lb-from-is",
        default=False,
        action="store_true",
        help="Get LBs from RunParams IS server, default is to generate fake LBs "
        "based on -r, -l, and -u options.",
    )
    parser.add_argument(
        "-P",
        "--runparam-part",
        default=def_part,
        metavar="STRING",
        help="Name of partition containing RunPArams IS server.",
    )
    parser.add_argument(
        "-r",
        "--run",
        default=1,
        metavar="NUMBER",
        type=int,
        help="Run number, def: %(default)s",
    )
    parser.add_argument(
        "-l",
        "--lbn",
        default=1,
        metavar="NUMBER",
        type=int,
        help="Starting LB number, def: %(default)s",
    )
    parser.add_argument(
        "-u",
        "--update",
        default=10.0,
        metavar="SECONDS",
        type=float,
        help="Update interval in seconds, same as LB length, def: %(default)s",
    )
    args = parser.parse_args()

    # init logging
    logging.basicConfig(level=logging.INFO)

    # instanciate IS object
    obj_name = args.server + "." + args.obj_name
    isobj = ispy.ISObject(args.partition, obj_name, "OnlPrefLumi")
    _LOG.info("Connected to IS object %s in partition %s", obj_name, args.partition)

    # make generator
    lbgen: Callable[[], Iterator[tuple[int, int, int, int]]]
    if args.lb_from_is:
        lbgen = _LBGenIS(args.runparam_part)
    else:
        lbgen = _FakeLBGen(args.run, args.lbn, args.update)

    # start looping
    abs_t0 = None
    for run, lbn, t0, t1 in lbgen():
        if abs_t0 is None:
            abs_t0 = t0

        # calculate new lumi and mu for this LB
        period = (t0 - abs_t0) / 1e9
        decay = math.exp(period / args.mean_lifetime)
        lumi = args.luminosity / decay
        mu = args.pileup / decay

        # bunches, some totaly fake data
        bunches = [0.0] * 3564
        for i in range(1, 101):
            bunches[i] = lumi / 100

        # fill all data members
        isobj.RunNumber = run
        isobj.LBN = lbn
        isobj.StartTime = t0
        isobj.EndTime = t1
        isobj.Channel = 0
        isobj.AlgorithmID = 201
        isobj.LBAvInstLumi = lumi
        isobj.LBAvEvtsPerBX = mu
        isobj.LumiType = 1
        isobj.Valid = 0
        isobj.BunchInstLumi = bunches

        # publish
        isobj.checkin()

        _LOG.info(
            "Published new values run=%s lbn=%s lumi=%s mu=%s t0=%s t1=%s",
            run,
            lbn,
            lumi,
            mu,
            t0,
            t1,
        )

        # maybe stop here
        if args.once:
            break

    return 0


if __name__ == "__main__":
    sys.exit(main())
