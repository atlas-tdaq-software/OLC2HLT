#!/bin/env python

"""Script which runs decider on a set of data from OnlPrefLumi folder.

Input to the script is the output of 'more /TRIGGER/LUMI/OnlPrefLumi'
in AtlCoolConsole. On output it will add "+" prefix to the lines which
cause updates. It assumes non-stable beams.

This software was developed for the ATLAS project.  If you use all or
part of it, please give an appropriate acknowledgement.

@author Andy Salnikov
"""

import collections
import logging
import re
import sys
from argparse import ArgumentParser

from pyolc2hlt.Decider import Decider
from pyolc2hlt.StableDecider import StableDecider
from pyolc2hlt.ThresholdDecider import ThresholdDecider

_logfmt = "%(asctime)s %(levelname)-8s %(message)s"


def _logLevels(lvl: int) -> int:
    if lvl == 0:
        return logging.WARNING
    if lvl == 1:
        return logging.INFO
    return logging.DEBUG


def _setLogging(lvl: int) -> None:
    rootlog = logging.getLogger()
    hdlr = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter(_logfmt)
    hdlr.setFormatter(formatter)
    rootlog.addHandler(hdlr)
    rootlog.setLevel(_logLevels(lvl))


_Data = collections.namedtuple("_Data", "LBAvInstLumi, StartTime, EndTime")

linere = re.compile(r"LBAvInstLumi \(Float\) : ([^\]]+)\]")


#
# Main function is called if the module is run as script
#
def main() -> int:
    """Run filtering algorithm."""
    # define options
    parser = ArgumentParser()
    parser.add_argument(
        "-v", dest="verbose", action="count", default=0, help="increase verbosity, can be used multiple times"
    )
    args = parser.parse_args()

    # setup logging based on verbosity level
    _setLogging(args.verbose)

    lumiChangePercent = 10
    unstableDeadBand = 0.05
    lumiChangePercentUnstable = 30
    minLBLength = 1
    sbeamFlag = lambda: False  # noqa: E731

    stableDecider = StableDecider(lumiChangePercent)
    unstableDecider = ThresholdDecider(unstableDeadBand, lumiChangePercentUnstable)
    decider = Decider(stableDecider, unstableDecider, minLBLength, sbeamFlag)

    t0 = 0
    oldLumi = None
    for line in sys.stdin:
        match = linere.search(line)
        if match is None:
            continue

        lumi = float(match.group(1))

        if oldLumi is None:
            oldLumi = lumi
            continue

        t1 = t0 + 60e9
        t2 = t1 + 60e9
        oldData = _Data(oldLumi, t0, t1)
        newData = _Data(lumi, t1, t2)
        decision = decider.decide(oldData, newData)
        # -- print oldData, newData, decision
        if decision:
            oldLumi = lumi
        t0 = t1

        if decision:
            line = "+ " + line
            # line = '\x1B[31m' + line + '\x1B[0m'
        else:
            line = "  " + line

        sys.stdout.write(line)

    return 0


if __name__ == "__main__":
    sys.exit(main())
