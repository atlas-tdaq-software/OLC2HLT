#!/bin/env tdaq_python
#
# PartitionMaker script to generate olc2hlt partition
#

import argparse
import os
import sys
from typing import Any

import pm
import pm.project
from config.dal import module as dal_module

# name of the IS server
_is_server_name = "OLC2HLT"

# input/output IS Object name
_in_is_object = _is_server_name + ".OnlPrefLumi-TestInput"
_out_is_object = _is_server_name + ".OnlPrefLumi"

# Tag name
_tag_name = os.environ["CMTCONFIG"]

# directory for log files (partition name will be added)
_log_root = "/logs/tdaq-06-01-01"


def main() -> int:
    """Create configuration for a segment."""
    parser = argparse.ArgumentParser(description="Generate segment or partition for OLC2HLT")
    parser.add_argument(
        "-p",
        "--partition",
        default="",
        metavar="STRING",
        help="if specified also generate partition containing OLC2HLT segment",
    )
    parser.add_argument(
        "-i",
        "--install-area",
        default=None,
        metavar="PATH",
        help="installation directory for OLC2HLT, if specified then software "
        "repository will be created for this area, otherwise use applications "
        "from existing tdaq release",
    )
    parser.add_argument(
        "-I",
        "--input-partition",
        default="",
        metavar="STRING",
        help="Partition with the input IS servers, default is the same partition as OLC2HLT",
    )
    parser.add_argument(
        "-O",
        "--output-partition",
        default="",
        metavar="STRING",
        help="Partition with the output IS servers, default is the same as input partition",
    )
    parser.add_argument(
        "-s",
        "--is-server",
        default=_is_server_name,
        metavar="STRING",
        help="output IS server name, def: " + _is_server_name,
    )
    parser.add_argument(
        "--input-object",
        default=_in_is_object,
        metavar="STRING",
        help="Name of the input IS object, def: " + _in_is_object,
    )
    parser.add_argument(
        "--output-object",
        default=_out_is_object,
        metavar="STRING",
        help="Name of the output IS object, def: " + _out_is_object,
    )
    parser.add_argument(
        "--cool-db",
        default="",
        metavar="STRING",
        help="Connection string for COOL database, default is to use standard name",
    )
    parser.add_argument(
        "-H",
        "--host",
        default="",
        metavar="HOST",
        help="optional host name where to run applications, def: <empty>=localhost",
    )
    parser.add_argument(
        "-t", "--tag", default=_tag_name, metavar="STRING", help="software tag name, def: " + _tag_name
    )
    parser.add_argument(
        "-l",
        "--log-root",
        default=_log_root,
        metavar="PATH",
        help="directory name for log files (not including partition name), def: " + _log_root,
    )
    parser.add_argument("output_file", help="The name of the output file")
    args = parser.parse_args()

    dal = dal_module("dal", "daq/schema/core.schema.xml")
    args.tag_dal = dal.Tag(args.tag)

    # Computer
    args.comp = None
    if args.host:
        hw_tag = "-".join(args.tag.split("-")[:2])
        args.comp = dal.Computer(args.host, HW_Tag=hw_tag)

    top_object, other_objects = make_segment(args)
    if args.partition:
        top_object = make_partition(top_object, args)

    includes = [
        "daq/schema/core.schema.xml",
        "daq/schema/olc2hlt.schema.xml",
        "daq/sw/tdaq-common-external-repository.data.xml",
        "daq/sw/repository.data.xml",
        "daq/segments/setup.data.xml",
    ]
    save_db = pm.project.Project(args.output_file, includes)
    save_db.addObjects([top_object])
    save_db.addObjects(other_objects)

    return 0


def make_partition(segment: Any, args: argparse.Namespace) -> Any:
    """Create partition wrapping given segment"""
    dal = dal_module("dal", "daq/schema/core.schema.xml")
    common_env = pm.project.Project("daq/segments/common-environment.data.xml")
    setup = pm.project.Project("daq/segments/setup.data.xml")

    # make partition object
    part = dal.Partition(
        args.partition,
        OnlineInfrastructure=setup.getObject("OnlineSegment", "setup"),
        Segments=[segment],
        DefaultHost=args.comp,
        DefaultTags=[args.tag_dal],
        LogRoot=args.log_root,
        WorkingDirectory="",
        Parameters=[common_env.getObject("VariableSet", "CommonParameters")],
    )

    return part


def make_segment(args: argparse.Namespace) -> Any:
    """Create OLC2HLT segment."""
    dal = dal_module("dal", "daq/schema/core.schema.xml")
    odal = dal_module("odal", "daq/schema/olc2hlt.schema.xml", [dal])
    repo = pm.project.Project("daq/sw/repository.data.xml")
    rc_pyrunner = repo.getObject("Binary", "rc_pyrunner")
    python_path = repo.getObject("Variable", "PYTHONPATH")

    # the are plenty of INFO messages produced by CoolUtils, do not
    # send them to MTS
    ers_info_stream = dal.Variable("OLC2HLT_INFO_STREAM", Name="TDAQ_ERS_INFO", Value="lstdout")

    if args.install_area:
        # make repository, extend PYTHONPATH
        pp = dal.SW_PackageVariable("PP", Name="PYTHONPATH", Suffix="share/lib/python")
        oscript = dal.Script("olc2hlt_script", BinaryName="olc2hlt.py")
        localrepo = dal.SW_Repository(
            "OLC2HLT_SW_Repo",
            Name="OLC2HLT_SW_Repo",
            InstallationPath=args.install_area,
            SW_Objects=[oscript],
            Tags=[args.tag_dal],
            Uses=[repo.getObject("SW_Repository", "Online")],
            AddProcessEnvironment=[pp],
            ISInfoDescriptionFiles=["OLC2HLT/olc2hlt_is_info.xml"],
        )
        oscript.BelongsTo = localrepo
    else:
        localrepo = None
        oscript = repo.getObject("Script", "olc2hlt.py")

    # IS server
    iss = dal.InfrastructureApplication(
        args.is_server,
        Parameters="-s ",
        RestartParameters="-s ",
        Program=repo.getObject("Binary", "is_server"),
        RestartableDuringRun=1,
    )

    # invalidator config
    sor_inv_cfg = odal.OLC2HLTInvalidateConfig(
        "olc2hlt-sor-invalidate",
        RunMode="SoR",
        LBNumber=0,
        Mu=20.0,
        Valid=2,
        ISPartition="",
        ISObject="RunParams.RunParams",
        MaxRunDiff=5000000,
        CoolDb=args.cool_db,
        CoolFolder="",
        CoolTag="",
        CoolChannel=0,
    )

    eor_inv_cfg = odal.OLC2HLTInvalidateConfig(
        "olc2hlt-eor-invalidate",
        RunMode="EoR",
        LBNumber=0,
        Mu=20.0,
        Valid=2,
        ISPartition="",
        ISObject="RunParams.RunParams",
        MaxRunDiff=5000000,
        CoolDb=args.cool_db,
        CoolFolder="",
        CoolTag="",
        CoolChannel=0,
    )

    # OLC2HLT config, id must be the same as ID of py_runner application
    config = odal.OLC2HLTConfig(
        "olc2hlt",
        SrcPartition=args.input_partition,
        SrcISInfo=args.input_object,
        DstPartition=args.output_partition,
        DstISInfo=args.output_object,
        LumiChangePercent=5.0,
        LumiChangePercentUnstable=50.0,
        UnstableDeadBand=0.05,
        MinLBLength=10,
        NoCTPUpdate=1,
        LogLevel="DEBUG",
        Log2ERS=False,  # for now do not want errors to confuse people
        SoRInvalidation=sor_inv_cfg,
        EoRInvalidation=eor_inv_cfg,
    )

    # OLC2HLT application (old style, non rc-controllable)
    # old_style_app = odal.OLC2HLTApp(
    #     'olc2hlt_app',
    #     Program=oscript,
    #     SrcPartition=args.input_partition,
    #     SrcISInfo=args.input_object,
    #     DstPartition=args.output_partition,
    #     DstISInfo=args.output_object,
    #     LumiChangePercent=5.,
    #     LumiChangePercentUnstable=50.,
    #     UnstableDeadBand=0.05,
    #     MinLBLength=10,
    #     NoCTPUpdate=1,
    #     LogLevel="DEBUG",
    #     Log2ERS=False  # for now do not want errors to confuse people
    # )

    # rc_pyrunner
    uses = []
    if localrepo:
        uses += [localrepo]
    oapp = dal.RunControlApplication(
        "olc2hlt",
        Program=rc_pyrunner,
        Parameters="-M Olc2HltControllable@pyolc2hlt.Olc2HltControllable",
        RestartParameters="-M Olc2HltControllable@pyolc2hlt.Olc2HltControllable",
        RestartableDuringRun=1,
        ProcessEnvironment=[ers_info_stream],
        InterfaceName="rc/controllable",
        Uses=uses,
    )

    # RC app for segment
    rcapp = dal.RunControlApplication(
        "olc2hlt_ctrl", Program=repo.getObject("Binary", "rc_controller"), InterfaceName="rc/controllable"
    )

    seg = dal.Segment(
        "OLC2HLT_Segment",
        IsControlledBy=rcapp,
        Applications=[oapp],
        Infrastructure=[iss],
        ProcessEnvironment=[python_path],
    )

    return seg, [sor_inv_cfg, eor_inv_cfg, config]


if __name__ == "__main__":
    sys.exit(main())
